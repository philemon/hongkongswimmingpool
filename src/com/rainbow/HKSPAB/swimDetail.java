package com.rainbow.HKSPAB;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

public class swimDetail extends Activity implements OnClickListener{
	static Context context;
	private static final String TAG = swimDetail.class.getName();
	private FuncGenerator generator;
	private SharedPreferences splanguage;
	private String language ="EN";
	private String mPath = "";
	private String siteID = "";
	private LinearLayout llSiteInfo, llInfo;
	//RelativeLayout rlGc;
	private ScrollView svInfo;
	private String latitude = "";
	private String longitude = "";
	private LinearLayout ll;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (savedInstanceState == null) {
			Bundle extras = getIntent().getExtras();
		    if(extras == null) {
		    	siteID = null;
		    } else {
		    	siteID = extras.getString("siteid");
		    }
		}
		generator = new FuncGenerator();  
		mPath = getString(R.string.database_path)+"/"+getString(R.string.database_name);
		initDataValue();
		initConfigLocale();
		initFunction();
	}
	
	
	private void initFunction() 
	{
		//generator.getAnalysis("SearchSwim");
		//generator.getAdvertisement(SearchSwim.this);
		getSPInfo(siteID);
	}
	
	private void initDataValue() 
	{
		context = swimDetail.this;
		//generator = new FuncGenerator();  
		splanguage = context.getSharedPreferences("LANGUAGE", MODE_PRIVATE);
		//setContentView(R.layout.details_swim);
	}
	
	private void initConfigLocale()
	{
		Configuration config = new Configuration(getResources().getConfiguration());
        if(splanguage.getInt("LANGUAGE_ID", -1)==1){
        	config.locale = Locale.ENGLISH;
        	language = "EN";
        }
        else if(splanguage.getInt("LANGUAGE_ID", -1)==2){
        	config.locale = Locale.CHINESE;
        	language = "CH";
        }else{
        	if(Locale.getDefault().getLanguage().equals("zh")){
        		config.locale = Locale.CHINESE;
        		language = "CH"; 
        	}
        }
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
	}
	
	private void getSPInfo(String siteID) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);	
		
		String sql = "SELECT * FROM hk_district, hk_region, site, site_location, sp_other_info, sp_open_schedule_summer WHERE ";
		sql += "site.site_id = sp_other_info.sp_id AND site.site_loc_id = site_location.loc_id AND ";
		sql += "site_location.loc_district = hk_district.district_id AND hk_district.region = hk_region.region_id AND ";
		sql += "sp_open_schedule_summer.sp_os_sm_id = sp_other_info.sp_open_schedule_summer_id AND site.site_id = '" + siteID + "';";
		
		/*String sql = "SELECT * FROM  ,  ,  ,  , site_photos , ";
		sql += "sp_facilities_indoor , sp_facilities_other , sp_facilities_outdoor ,  , ";
		sql += "sp_open_schedule_winter , ";
		sql += " WHERE site.site_id = sp_other_info.sp_id AND site.site_loc_id = site_location.loc_id AND site.site_photo_id = site_photos.site_photo_id";
		sql += " AND site_location.loc_district = hk_district.district_id AND hk_district.region = hk_region.region_id";
		sql += " AND sp_facilities_indoor.sp_fac_in_id = sp_other_info.sp_fac_indoor_id AND ";
		sql += "sp_facilities_outdoor.sp_fac_out_id = sp_other_info.sp_fac_outdoor_id AND ";
		sql += "sp_facilities_other.sp_fac_other_id = sp_other_info.sp_fac_other_id AND ";
		sql += "sp_open_schedule_summer.sp_os_sm_id = sp_other_info.sp_open_schedule_summer_id AND ";
		sql += "sp_open_schedule_winter.sp_os_wt_id = sp_other_info.sp_open_schedule_winter_id AND site.site_id = '" + siteID + "';";
		*/
		Log.e("sql", sql);
		
		//Cursor cursor = db.rawQuery(sql, null);
		//cursor.moveToFirst();
		//int count= cursor.getInt(0);
		//Log.e("count", count+"");
		//siteid = new String[count];
		//sitename = new String[count];
		//sql = "SELECT * FROM site , site_location , hk_district , sp_other_info WHERE SP_ID = SITE_ID AND SITE_LOC_ID = LOC_ID AND LOC_DISTRICT = DISTRICT_ID AND SITE_TYPE = '"+sitetype+"' "+ where+" ;";
		Cursor cursor = db.rawQuery(sql, null);
		//int countnum = 0;
		if( cursor == null || cursor.getCount() == 0){
			Log.e(TAG, "cursor == null!");
			return;
		}
		cursor.moveToNext();
		siteInfo(cursor.getString(cursor.getColumnIndex("SITE_NAME_" + "EN")),
				cursor.getString(cursor.getColumnIndex("SITE_NAME_" + language)), 
				cursor.getString(cursor.getColumnIndex("DISTRICT_DESC_" + language)), 
				cursor.getString(cursor.getColumnIndex("REGION_DESC_" + language)), 
				cursor.getString(cursor.getColumnIndex("LOC_ADDRESS_" + language)), 
				cursor.getString(cursor.getColumnIndex("SITE_TEL")), 
				cursor.getString(cursor.getColumnIndex("SITE_OFFICE_TEL")), 
				cursor.getString(cursor.getColumnIndex("SP_WEEKLY_CLEANS_OPEAR")), 
				cursor.getString(cursor.getColumnIndex("SP_HOLIDAY_CLEANS_OPEAR")), 
				cursor.getString(cursor.getColumnIndex("SP_MAINTENANCE_DESC_" + language)), 
				cursor.getString(cursor.getColumnIndex("SP_OPEN_SCHEDULE_SUMMER_ID")), 
				cursor.getString(cursor.getColumnIndex("SP_OPEN_SCHEDULE_WINTER_ID")), 
				cursor.getString(cursor.getColumnIndex("SP_FAC_INDOOR_ID")), 
				cursor.getString(cursor.getColumnIndex("SP_FAC_OUTDOOR_ID")), 
				cursor.getString(cursor.getColumnIndex("SP_FAC_OTHER_ID")));
		
		latitude = cursor.getString(cursor.getColumnIndex("LOC_LATITUDE"));
		longitude = cursor.getString(cursor.getColumnIndex("LOC_LONGITUDE"));
			//Log.e("SID", cursor.getString(cursor.getColumnIndex("SP_OPEN_SCHEDULE_WINTER_ID")));
			//Log.e("siteid", cursor.getString(cursor.getColumnIndex("SITE_ID")));
			//siteid[countnum] = cursor.getString(cursor.getColumnIndex("SITE_ID"));
			//sitename[countnum] = cursor.getString(cursor.getColumnIndex("SITE_NAME_"+language));
			//Log.e("cursor", siteid[countnum] + " " + sitename[countnum]);
			//++countnum;
	}
	
	private String chkWeek(String chkday) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);	
		
		String sql = "SELECT * FROM week WHERE WEEK_ID = '" + chkday + "'";
		//Log.e("sql", sql);
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		return cursor.getString(cursor.getColumnIndex("WEEK_DESC_" + language));

	}
	
	private String chkSumSch(String sumID) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);	
		
		String sql = "SELECT * FROM SP_OPEN_SCHEDULE_SUMMER WHERE SP_OS_SM_ID = '" + sumID + "'";
		//Log.e("sql", sql);
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		return cursor.getString(cursor.getColumnIndex("SP_OS_SM_DESC_" + language));

	}
	
	public void sumInfo(String sumDesc){
		TextView tvSum = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvSum.setText(getString(R.string.swim_opening_schedule) + ": " + sumDesc);
		tvSum.setTextSize(18);
		tvSum.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvSum.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvSum.setBackgroundResource(R.color.white);
		tvSum.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvSum);

		View line = new View(context); line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
	}
	
	private String chkWinSch(String winID) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);	
		
		String sql = "SELECT * FROM SP_OPEN_SCHEDULE_WINTER WHERE SP_OS_WT_ID = '" + winID + "'";
		//Log.e("sql", sql);
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		return cursor.getString(cursor.getColumnIndex("SP_OS_WT_DESC_" + language));

	}
	
	public void winInfo(String winDesc){
		TextView tvWin = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvWin.setText(getString(R.string.swim_opening_schedule) + ": " + winDesc);
		tvWin.setTextSize(18);
		tvWin.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvWin.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvWin.setBackgroundResource(R.color.white);
		tvWin.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvWin);

		View line = new View(context); line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
	}
	
	private void chkIndoor(String inID) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);	
		
		String sql = "SELECT * FROM SP_FACILITIES_INDOOR WHERE SP_FAC_IN_ID = '" + inID + "'";
		//Log.e("sql", sql);
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		
		inInfo(cursor.getString(cursor.getColumnIndex("MAIN_POOL")), 
				cursor.getString(cursor.getColumnIndex("SECONDARY_POOL")), 
				cursor.getString(cursor.getColumnIndex("TRANING_POOL")), 
				cursor.getString(cursor.getColumnIndex("TEACHING_POOL")), 
				cursor.getString(cursor.getColumnIndex("DIVING_POOL")), 
				cursor.getString(cursor.getColumnIndex("CHILDREN_POOL")), 
				cursor.getString(cursor.getColumnIndex("TODDLER_POOL")), 
				cursor.getString(cursor.getColumnIndex("LEISURE_POOL")), 
				cursor.getString(cursor.getColumnIndex("SPECTATOR_STAND")), 
				cursor.getString(cursor.getColumnIndex("ELEC_SCOREBOARD")), 
				cursor.getString(cursor.getColumnIndex("FAMILY_CHG_RM")), 
				cursor.getString(cursor.getColumnIndex("JACUZZI")));
		
		/*siteInfo(cursor.getString(cursor.getColumnIndex("SITE_NAME_" + language)), 
		cursor.getString(cursor.getColumnIndex("DISTRICT_DESC_" + language)), 
		cursor.getString(cursor.getColumnIndex("REGION_DESC_" + language)), 
		cursor.getString(cursor.getColumnIndex("LOC_ADDRESS_" + language)), 
		cursor.getString(cursor.getColumnIndex("SITE_TEL")), 
		cursor.getString(cursor.getColumnIndex("SITE_OFFICE_TEL")), 
		cursor.getString(cursor.getColumnIndex("SP_WEEKLY_CLEANS_OPEAR")), 
		cursor.getString(cursor.getColumnIndex("SP_HOLIDAY_CLEANS_OPEAR")), 
		cursor.getString(cursor.getColumnIndex("SP_MAINTENANCE_DESC_" + language)), 
		cursor.getString(cursor.getColumnIndex("SP_OPEN_SCHEDULE_SUMMER_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_OPEN_SCHEDULE_WINTER_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_FAC_INDOOR_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_FAC_OUTDOOR_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_FAC_OTHER_ID")));*/

	}
	
	public void inInfo(String mainPool, String secPool, String traPool, String teaPool, String divPool, String chiPool, 
			String todPool, String leiPool, String SpeStand, String eleSBoa, String famRm, String jac){
		TextView tvMain = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvMain.setText(getString(R.string.mainPool) + ": " + mainPool);
		tvMain.setTextSize(18);
		tvMain.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvMain.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvMain.setBackgroundResource(R.color.white);
		tvMain.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvMain);

		View line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvSec = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvSec.setText(getString(R.string.secPool) + ": " + secPool);
		tvSec.setTextSize(18);
		tvSec.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvSec.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvSec.setBackgroundResource(R.color.white);
		tvSec.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvSec);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvTra = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvTra.setText(getString(R.string.traPool) + ": " + traPool);
		tvTra.setTextSize(18);
		tvTra.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvTra.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvTra.setBackgroundResource(R.color.white);
		tvTra.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvTra);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvTea = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvTea.setText(getString(R.string.teaPool) + ": " + teaPool);
		tvTea.setTextSize(18);
		tvTea.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvTea.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvTea.setBackgroundResource(R.color.white);
		tvTea.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvTea);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvDiv = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvDiv.setText(getString(R.string.divPool) + ": " + divPool);
		tvDiv.setTextSize(18);
		tvDiv.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvDiv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvDiv.setBackgroundResource(R.color.white);
		tvDiv.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvDiv);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvChi = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvChi.setText(getString(R.string.chiPool) + ": " + chiPool);
		tvChi.setTextSize(18);
		tvChi.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvChi.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvChi.setBackgroundResource(R.color.white);
		tvChi.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvChi);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvtod = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvtod.setText(getString(R.string.todPool) + ": " + todPool);
		tvtod.setTextSize(18);
		tvtod.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvtod.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvtod.setBackgroundResource(R.color.white);
		tvtod.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvtod);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvlei = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvlei.setText(getString(R.string.leiPool) + ": " + leiPool);
		tvlei.setTextSize(18);
		tvlei.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvlei.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvlei.setBackgroundResource(R.color.white);
		tvlei.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvlei);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvSpeSta = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvSpeSta.setText(getString(R.string.speSta) + ": " + SpeStand);
		tvSpeSta.setTextSize(18);
		tvSpeSta.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvSpeSta.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvSpeSta.setBackgroundResource(R.color.white);
		tvSpeSta.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvSpeSta);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tveleSBoa = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tveleSBoa.setText(getString(R.string.eleSBoa) + ": " + eleSBoa);
		tveleSBoa.setTextSize(18);
		tveleSBoa.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tveleSBoa.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tveleSBoa.setBackgroundResource(R.color.white);
		tveleSBoa.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tveleSBoa);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvfamRm = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvfamRm.setText(getString(R.string.famRm) + ": " + famRm);
		tvfamRm.setTextSize(18);
		tvfamRm.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvfamRm.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvfamRm.setBackgroundResource(R.color.white);
		tvfamRm.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvfamRm);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvjac = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvjac.setText(getString(R.string.jac) + ": " + jac);
		tvjac.setTextSize(18);
		tvjac.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvjac.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvjac.setBackgroundResource(R.color.white);
		tvjac.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvjac);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
	}
	
	private void chkOutdoor(String outID) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);	
		
		String sql = "SELECT * FROM SP_FACILITIES_OUTDOOR WHERE SP_FAC_OUT_ID = '" + outID + "'";
		//Log.e("sql", sql);
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		
		outInfo(cursor.getString(cursor.getColumnIndex("MAIN_POOL")), 
				cursor.getString(cursor.getColumnIndex("SECONDARY_POOL")), 
				cursor.getString(cursor.getColumnIndex("TRANING_POOL")), 
				cursor.getString(cursor.getColumnIndex("TEACHING_POOL")), 
				cursor.getString(cursor.getColumnIndex("DIVING_POOL")), 
				cursor.getString(cursor.getColumnIndex("CHILDREN_POOL")), 
				cursor.getString(cursor.getColumnIndex("TODDLER_POOL")), 
				cursor.getString(cursor.getColumnIndex("LEISURE_POOL")), 
				cursor.getString(cursor.getColumnIndex("SPECTATOR_STAND")), 
				cursor.getString(cursor.getColumnIndex("ELEC_SCOREBOARD")), 
				cursor.getString(cursor.getColumnIndex("FAMILY_CHG_RM")), 
				cursor.getString(cursor.getColumnIndex("JACUZZI")), 
				cursor.getString(cursor.getColumnIndex("PARTICIPATORY_FOUNTAIN")), 
				cursor.getString(cursor.getColumnIndex("SUN_BATHING_AREA")));
		
		/*siteInfo(cursor.getString(cursor.getColumnIndex("SITE_NAME_" + language)), 
		cursor.getString(cursor.getColumnIndex("DISTRICT_DESC_" + language)), 
		cursor.getString(cursor.getColumnIndex("REGION_DESC_" + language)), 
		cursor.getString(cursor.getColumnIndex("LOC_ADDRESS_" + language)), 
		cursor.getString(cursor.getColumnIndex("SITE_TEL")), 
		cursor.getString(cursor.getColumnIndex("SITE_OFFICE_TEL")), 
		cursor.getString(cursor.getColumnIndex("SP_WEEKLY_CLEANS_OPEAR")), 
		cursor.getString(cursor.getColumnIndex("SP_HOLIDAY_CLEANS_OPEAR")), 
		cursor.getString(cursor.getColumnIndex("SP_MAINTENANCE_DESC_" + language)), 
		cursor.getString(cursor.getColumnIndex("SP_OPEN_SCHEDULE_SUMMER_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_OPEN_SCHEDULE_WINTER_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_FAC_INDOOR_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_FAC_OUTDOOR_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_FAC_OTHER_ID")));*/

	}
	
	public void outInfo(String mainPool, String secPool, String traPool, String teaPool, String divPool, String chiPool, 
			String todPool, String leiPool, String SpeStand, String eleSBoa, String famRm, String jac, String parFou, String sunBath){
		TextView tvMain = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvMain.setText(getString(R.string.mainPool) + ": " + mainPool);
		tvMain.setTextSize(18);
		tvMain.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvMain.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvMain.setBackgroundResource(R.color.white);
		tvMain.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvMain);

		View line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvSec = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvSec.setText(getString(R.string.secPool) + ": " + secPool);
		tvSec.setTextSize(18);
		tvSec.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvSec.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvSec.setBackgroundResource(R.color.white);
		tvSec.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvSec);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvTra = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvTra.setText(getString(R.string.traPool) + ": " + traPool);
		tvTra.setTextSize(18);
		tvTra.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvTra.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvTra.setBackgroundResource(R.color.white);
		tvTra.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvTra);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvTea = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvTea.setText(getString(R.string.teaPool) + ": " + teaPool);
		tvTea.setTextSize(18);
		tvTea.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvTea.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvTea.setBackgroundResource(R.color.white);
		tvTea.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvTea);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvDiv = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvDiv.setText(getString(R.string.divPool) + ": " + divPool);
		tvDiv.setTextSize(18);
		tvDiv.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvDiv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvDiv.setBackgroundResource(R.color.white);
		tvDiv.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvDiv);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvChi = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvChi.setText(getString(R.string.chiPool) + ": " + chiPool);
		tvChi.setTextSize(18);
		tvChi.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvChi.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvChi.setBackgroundResource(R.color.white);
		tvChi.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvChi);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvtod = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvtod.setText(getString(R.string.todPool) + ": " + todPool);
		tvtod.setTextSize(18);
		tvtod.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvtod.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvtod.setBackgroundResource(R.color.white);
		tvtod.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvtod);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvlei = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvlei.setText(getString(R.string.leiPool) + ": " + leiPool);
		tvlei.setTextSize(18);
		tvlei.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvlei.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvlei.setBackgroundResource(R.color.white);
		tvlei.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvlei);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvSpeSta = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvSpeSta.setText(getString(R.string.speSta) + ": " + SpeStand);
		tvSpeSta.setTextSize(18);
		tvSpeSta.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvSpeSta.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvSpeSta.setBackgroundResource(R.color.white);
		tvSpeSta.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvSpeSta);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tveleSBoa = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tveleSBoa.setText(getString(R.string.eleSBoa) + ": " + eleSBoa);
		tveleSBoa.setTextSize(18);
		tveleSBoa.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tveleSBoa.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tveleSBoa.setBackgroundResource(R.color.white);
		tveleSBoa.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tveleSBoa);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvfamRm = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvfamRm.setText(getString(R.string.famRm) + ": " + famRm);
		tvfamRm.setTextSize(18);
		tvfamRm.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvfamRm.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvfamRm.setBackgroundResource(R.color.white);
		tvfamRm.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvfamRm);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvjac = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvjac.setText(getString(R.string.jac) + ": " + jac);
		tvjac.setTextSize(18);
		tvjac.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvjac.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvjac.setBackgroundResource(R.color.white);
		tvjac.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvjac);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvParFou = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvParFou.setText(getString(R.string.par_fou) + ": " + parFou);
		tvParFou.setTextSize(18);
		tvParFou.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvParFou.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvParFou.setBackgroundResource(R.color.white);
		tvParFou.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvParFou);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvSunBath = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvSunBath.setText(getString(R.string.sun_bath) + ": " + sunBath);
		tvSunBath.setTextSize(18);
		tvSunBath.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvSunBath.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvSunBath.setBackgroundResource(R.color.white);
		tvSunBath.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvSunBath);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
	}
	
	private void chkOtherdoor(String otherID) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);	
		
		String sql = "SELECT * FROM SP_FACILITIES_OTHER WHERE SP_FAC_OTHER_ID = '" + otherID + "'";
		//Log.e("sql", sql);
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		
		otherInfo(cursor.getString(cursor.getColumnIndex("DISAB_BFA")), 
				cursor.getString(cursor.getColumnIndex("DISAB_CHG_RM")), 
				cursor.getString(cursor.getColumnIndex("DISAB_TOILET")), 
				cursor.getString(cursor.getColumnIndex("FAC_PARKING")), 
				cursor.getString(cursor.getColumnIndex("FAC_INC_DISAB_PARKING")));
		
		/*siteInfo(cursor.getString(cursor.getColumnIndex("SITE_NAME_" + language)), 
		cursor.getString(cursor.getColumnIndex("DISTRICT_DESC_" + language)), 
		cursor.getString(cursor.getColumnIndex("REGION_DESC_" + language)), 
		cursor.getString(cursor.getColumnIndex("LOC_ADDRESS_" + language)), 
		cursor.getString(cursor.getColumnIndex("SITE_TEL")), 
		cursor.getString(cursor.getColumnIndex("SITE_OFFICE_TEL")), 
		cursor.getString(cursor.getColumnIndex("SP_WEEKLY_CLEANS_OPEAR")), 
		cursor.getString(cursor.getColumnIndex("SP_HOLIDAY_CLEANS_OPEAR")), 
		cursor.getString(cursor.getColumnIndex("SP_MAINTENANCE_DESC_" + language)), 
		cursor.getString(cursor.getColumnIndex("SP_OPEN_SCHEDULE_SUMMER_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_OPEN_SCHEDULE_WINTER_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_FAC_INDOOR_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_FAC_OUTDOOR_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_FAC_OTHER_ID")));*/

	}
	
	public void otherInfo(String bfa, String chgRm, String toilet, String facPar, String facDisPar){
		TextView tvBFA = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		if(bfa.equals("1"))
			tvBFA.setText(getString(R.string.bfa) + ": " + getString(R.string.bfa_yes));
		else
			tvBFA.setText(getString(R.string.bfa) + ": " + getString(R.string.bfa_no));
		
		tvBFA.setTextSize(18);
		tvBFA.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvBFA.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvBFA.setBackgroundResource(R.color.white);
		tvBFA.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvBFA);

		View line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvChgRm = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvChgRm.setText(getString(R.string.chg_rm) + ": " + chgRm + " (" + getString(R.string.disab) + ")");
		tvChgRm.setTextSize(18);
		tvChgRm.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvChgRm.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvChgRm.setBackgroundResource(R.color.white);
		tvChgRm.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvChgRm);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvToilet = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvToilet.setText(getString(R.string.toilet) + ": " + toilet + " (" + getString(R.string.disab) + ")");
		tvToilet.setTextSize(18);
		tvToilet.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvToilet.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvToilet.setBackgroundResource(R.color.white);
		tvToilet.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvToilet);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvFacPar = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvFacPar.setText(getString(R.string.fac_par) + ": " + facPar);
		tvFacPar.setTextSize(18);
		tvFacPar.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvFacPar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvFacPar.setBackgroundResource(R.color.white);
		tvFacPar.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvFacPar);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvFacDisPar = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvFacDisPar.setText(getString(R.string.fac_dis_par) + ": " + facDisPar + " (" + getString(R.string.disab) + ")");
		tvFacDisPar.setTextSize(18);
		tvFacDisPar.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvFacDisPar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvFacDisPar.setBackgroundResource(R.color.white);
		tvFacDisPar.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvFacDisPar);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
	}
	
	public void siteInfo(String siteEngName, String siteName, String districtName, String regionName, String location, String tel, String officeTel, String cleanDay, String cleanHDay, 
			String maintenance, String sumSch, String winSch, String indoorF, String outdoorF, String otherF){
		generator.getAnalysis(siteEngName);
		
		llInfo = new LinearLayout(this);
		llInfo.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
		llInfo.setOrientation(1);
		
		llSiteInfo = new LinearLayout(this);
		llSiteInfo.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
		llSiteInfo.setOrientation(1);
		
		svInfo = new ScrollView(this);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT,1);
		svInfo.setLayoutParams(params);
		
		TextView tvSiteName = new TextView(this);
		tvSiteName.setText(siteName);
		tvSiteName.setTextSize(25);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvSiteName.setGravity(Gravity.CENTER);
		tvSiteName.setBackgroundResource(R.color.highLightBlue);
		tvSiteName.setTextColor(getResources().getColor(R.color.black));
		//llSiteInfo.addView(tvSiteName);

		TextView tvBInfo = new TextView(this);
		tvBInfo.setText(getString(R.string.bInfo));
		tvBInfo.setTextSize(22);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvBInfo.setGravity(Gravity.CENTER);
		tvBInfo.setBackgroundResource(R.color.titleBlue);
		tvBInfo.setTextColor(getResources().getColor(R.color.black));
		llSiteInfo.addView(tvBInfo);

		View line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvDistReg = new TextView(this);
		tvDistReg.setText(getString(R.string.district) + ": " + districtName + ", " + regionName);
		tvDistReg.setTextSize(18);
		tvDistReg.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvDistReg.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvDistReg.setBackgroundResource(R.color.white);
		tvDistReg.setTextColor(getResources().getColor(R.color.black));
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvDistReg);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvLocation = new TextView(this);

		String siteadd = getString(R.string.swim_address) + ": ";
		Spannable towedToAddress = new SpannableString(siteadd + location);
        towedToAddress.setSpan(new UnderlineSpan(), 4,towedToAddress.length(), 4);
        towedToAddress.setSpan(new ForegroundColorSpan(0xFF2894FF), 4, towedToAddress.length(), 4);
		
		tvLocation.setText(towedToAddress);
		tvLocation.setTextSize(18);
		tvLocation.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvLocation.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		//tvLocation.setBackgroundResource(R.color.white);
		//tvLocation.setTextColor(getResources().getColor(R.color.black));
		tvLocation.setOnClickListener(swimDetail.this);
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvLocation);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvTel = new TextView(this);
		//tvTel.setText(getString(R.string) + ": " + tel);
		if(officeTel.equals("null"))
			tvTel.setText(getString(R.string.tel) + ": " + tel);
		else
			tvTel.setText(getString(R.string.tel) + ": " + tel + " / " + officeTel);
			
		tvTel.setTextSize(18);
		tvTel.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvTel.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvTel.setBackgroundResource(R.color.white);
		tvTel.setTextColor(getResources().getColor(R.color.black));
		Linkify.addLinks(tvTel, Linkify.PHONE_NUMBERS);
		tvTel.setLinkTextColor(Color.parseColor("#2894FF"));
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvTel);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
			//Log.e("Er", "error");
		
		TextView tvClean = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvClean.setText(getString(R.string.clean_day) + " (" + getString(R.string.normal_day) + "): " + chkWeek(cleanDay));
		tvClean.setTextSize(18);
		tvClean.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvClean.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvClean.setBackgroundResource(R.color.white);
		tvClean.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvClean);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvCleanH = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvCleanH.setText(getString(R.string.clean_day) + " (" + getString(R.string.holiday_day) + "): " + chkWeek(cleanHDay));
		tvCleanH.setTextSize(18);
		tvCleanH.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvCleanH.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvCleanH.setBackgroundResource(R.color.white);
		tvCleanH.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvCleanH);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvMaintenance = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvMaintenance.setText(getString(R.string.swim_maintenance) + ": " + maintenance);
		tvMaintenance.setTextSize(18);
		tvMaintenance.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvMaintenance.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvMaintenance.setBackgroundResource(R.color.white);
		tvMaintenance.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvMaintenance);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvSumInfo = new TextView(this);
		tvSumInfo.setText(getString(R.string.sumInfo));
		tvSumInfo.setTextSize(22);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvSumInfo.setGravity(Gravity.CENTER);		
		
		if(sumSch.equals("null")){
			tvSumInfo.setBackgroundResource(R.color.gray);
			tvSumInfo.setTextColor(getResources().getColor(R.color.white));
			llSiteInfo.addView(tvSumInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		}
		else{
			tvSumInfo.setBackgroundResource(R.color.titleBlue);
			tvSumInfo.setTextColor(getResources().getColor(R.color.black));
			llSiteInfo.addView(tvSumInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
			//tvSumInfo.setOnClickListener(this);
			sumInfo(chkSumSch(sumSch));
		}
		
		
		TextView tvWinInfo = new TextView(this);
		tvWinInfo.setText(getString(R.string.winInfo));
		tvWinInfo.setTextSize(22);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvWinInfo.setGravity(Gravity.CENTER);		
		
		if(winSch.equals("null")){
			tvWinInfo.setBackgroundResource(R.color.gray);
			tvWinInfo.setTextColor(getResources().getColor(R.color.white));
			llSiteInfo.addView(tvWinInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		}
		else{
			tvWinInfo.setBackgroundResource(R.color.titleBlue);
			tvWinInfo.setTextColor(getResources().getColor(R.color.black));
			llSiteInfo.addView(tvWinInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
			winInfo(chkWinSch(winSch));
		}

		TextView tvIndoorInfo = new TextView(this);
		tvIndoorInfo.setText(getString(R.string.indoor));
		tvIndoorInfo.setTextSize(22);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvIndoorInfo.setGravity(Gravity.CENTER);		
		
		if(indoorF.equals("null")){
			tvIndoorInfo.setBackgroundResource(R.color.gray);
			tvIndoorInfo.setTextColor(getResources().getColor(R.color.white));
			llSiteInfo.addView(tvIndoorInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		}
		else{
			tvIndoorInfo.setBackgroundResource(R.color.titleBlue);
			tvIndoorInfo.setTextColor(getResources().getColor(R.color.black));
			llSiteInfo.addView(tvIndoorInfo);
			line = new View(context); line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
			chkIndoor(indoorF);
		}
		
		TextView tvOutdoorInfo = new TextView(this);
		tvOutdoorInfo.setText(getString(R.string.outdoor));
		tvOutdoorInfo.setTextSize(22);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvOutdoorInfo.setGravity(Gravity.CENTER);		
		
		if(outdoorF.equals("null")){
			tvOutdoorInfo.setBackgroundResource(R.color.gray);
			tvOutdoorInfo.setTextColor(getResources().getColor(R.color.white));
			llSiteInfo.addView(tvOutdoorInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		}
		else{
			tvOutdoorInfo.setBackgroundResource(R.color.titleBlue);
			tvOutdoorInfo.setTextColor(getResources().getColor(R.color.black));
			llSiteInfo.addView(tvOutdoorInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
			chkOutdoor(outdoorF);
		}

		TextView tvOtherInfo = new TextView(this);
		tvOtherInfo.setText(getString(R.string.otherF));
		tvOtherInfo.setTextSize(22);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvOtherInfo.setGravity(Gravity.CENTER);		
		
		if(otherF.equals("null")){
			tvOtherInfo.setBackgroundResource(R.color.gray);
			tvOtherInfo.setTextColor(getResources().getColor(R.color.white));
			llSiteInfo.addView(tvOtherInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		}
		else{
			tvOtherInfo.setBackgroundResource(R.color.titleBlue);
			tvOtherInfo.setTextColor(getResources().getColor(R.color.black));
			llSiteInfo.addView(tvOtherInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
			chkOtherdoor(otherF);
		}
		if(generator.isOnline()){
			ll = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.details_swim, null);
			ImageView ivSwim = (ImageView) ll.findViewById(R.id.ivSwim);
	        String urldisplay = getString(R.string.swim_image_url) + siteEngName.replace("Swimming Pool", "").replaceAll(" ", "%20")+"SP.jpg";
			new DownloadImageTask(ivSwim).execute(urldisplay);
		}
		svInfo.addView(llSiteInfo);
		llInfo.addView(tvSiteName);
		llInfo.addView(svInfo);
		setContentView(llInfo);
	}
	
	class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	      ImageView bmImage;

	      public DownloadImageTask(ImageView bmImage) {
	          this.bmImage = bmImage;
	      }

	      protected Bitmap doInBackground(String... urls) {
	          String urldisplay = urls[0];
	          Bitmap mIcon11 = null;
	          try {
	            InputStream in = new java.net.URL(urldisplay).openStream();
	            mIcon11 = BitmapFactory.decodeStream(in);
	          } catch (Exception e) {
	              Log.e("Error", e.getMessage());
	          }
	          return mIcon11;
	      }

	      protected void onPostExecute(Bitmap result) {
	          if(result != null){
		          bmImage.setImageBitmap(result);
		          llSiteInfo.addView(ll);
	          }
	      }
	    }
	@Override
	public void onClick(View v) {
		Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + latitude + "," + longitude + "?q=<" + latitude + ">,<" + longitude + ">&z=18"));
	    startActivity(searchAddress);
		
	}
}
