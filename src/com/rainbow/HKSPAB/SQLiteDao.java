package com.rainbow.HKSPAB;

import org.json.JSONException;
import org.json.JSONObject;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

//SQLiteDao = SQLiteDataAccessObject
public class SQLiteDao
{
	private static final String TAG = SQLiteDao.class.getName();
	private static String path = MyApplication.getAppContext().getString(R.string.database_path)+"/"+
								 MyApplication.getAppContext().getString(R.string.database_name);
	private static SQLiteDatabase db = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.CREATE_IF_NECESSARY);		
	
	protected static boolean checkTableExists(String tablename) 
	{
		try
		{
		    db.query(tablename, null, null, null, null, null, null);
		    return true;
		}
		catch (SQLException e) 
		{
		    Log.d(TAG, tablename+ " doesn't exist :(((");
			return false;
		    /* fail */
		}
	}
	
	protected static class Create
	{			
		protected static void createBeachIndoorFacTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `bh_facilities_indoor` ("+
							"`BH_FAC_IN_ID` varchar(6) PRIMARY KEY NOT NULL," +
							"`ACTIVITY_RM` bit(1) NOT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createBHOutdoorFacTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `bh_facilities_outdoor` ("+
							"`BH_FAC_OUT_ID` varchar(6) PRIMARY KEY NOT NULL,"+
							"`FAST_FOOD_KIOSK` bit(1) NOT NULL,"+
							"`LIGHT_REFRESHMENT_KIOSK` bit(1) NOT NULL,"+
							"`RESTAURANT` bit(1) NOT NULL,"+
							"`BBQ_PIT` bit(11) NOT NULL,"+
							"`CHG_RM` bit(1) NOT NULL,"+
							"`SHOWER_FAC` bit(1) NOT NULL,"+
							"`TOILET` bit(1) NOT NULL,"+
							"`BATHING_SHED` bit(1) NOT NULL,"+
							"`RAFT` bit(1) NOT NULL,"+
							"`BH_VB_COURT` bit(1) NOT NULL,"+
							"`PLAYGROUND` bit(1) NOT NULL,"+
							"`GOLF_COURSE` bit(1) NOT NULL,"+
							"`FITNESS_EQUIP_FOR_ELDERLY` bit(1) NOT NULL,"+
							"`FEE_CHARGING_CAR_PARK` bit(1) NOT NULL,"+
							"`FAC_INC_DISAB_PARKING` bit(1) NOT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createBHOtherInfoTable()
		{
			String sql = 	"CREATE TABLE IF NOT EXISTS `bh_other_info` ("+
							"`BH_ID` varchar(4) NOT NULL DEFAULT 'BH',"+
							"`BH_FAC_INDOOR_ID` varchar(6) DEFAULT NULL,"+
							"`BH_FAC_OUTDOOR_ID` varchar(6) DEFAULT NULL,"+
							"`BH_LG_SERV_TIME_SM_ID` varchar(6) DEFAULT NULL,"+
							"`BH_LG_SERV_TIME_WT_ID` varchar(6) DEFAULT NULL,"+
							"`IS_PUBLIC` bit(1) NOT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createBHLGServSMTimeTable()
		{
			String sql 	= 	"CREATE TABLE IF NOT EXISTS `bh_lifeguard_service_time_summer` ("+
							"`BH_LG_ST_SM_ID` varchar(6) PRIMARY KEY NOT NULL,"+
							"`BH_LG_ST_SM_DESC_CH` varchar(100) NOT NULL,"+
							"`BH_LG_ST_SM_DESC_EN` varchar(250) NOT NULL,"+
							"`BH_LG_ST_SM_REMARK` varchar(250) DEFAULT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createBHLGServWTTimeTable()
		{
			String sql = 	"CREATE TABLE IF NOT EXISTS `bh_lifeguard_service_time_winter` ("+
							"`BH_LG_ST_WT_ID` varchar(6) PRIMARY KEY NOT NULL,"+
							"`BH_LG_ST_WT_DESC_CH` varchar(100) NOT NULL,"+
							"`BH_LG_ST_WT_DESC_EN` varchar(250) NOT NULL,"+
							"`BH_LG_ST_WT_REMARK` varchar(250) DEFAULT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createHKDistrictTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `hk_district` ("+
							"`DISTRICT_ID` varchar(3) PRIMARY KEY NOT NULL,"+
						  	"`REGION` varchar(2) NOT NULL,"+
						  	"`DISTRICT_DESC_CH` varchar(3) NOT NULL,"+
						  	"`DISTRICT_DESC_EN` varchar(25) NOT NULL);";
			db.execSQL(sql);	
		}
		
		protected static void createHKRegionTable()
		{
			String sql = 	"CREATE TABLE IF NOT EXISTS `hk_region` ("+
						 	"`REGION_ID` varchar(2) PRIMARY KEY NOT NULL,"+
						 	"`REGION_DESC_CH` varchar(2) NOT NULL,"+
						 	"`REGION_DESC_EN` varchar(25) NOT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createSiteTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS 'site' ("+
							"`SITE_ID` varchar(4) PRIMARY KEY NOT NULL," +
							"`SITE_TYPE` varchar(2) NOT NULL,"+
							"`SITE_LOC_ID` varchar(3) NOT NULL,"+
							" `SITE_PHOTO_ID` varchar(3) NOT NULL,"+
							"`SITE_NAME_CH` varchar(15) NOT NULL,"+
							"`SITE_NAME_EN` varchar(80) NOT NULL,"+
							"`SITE_TEL` varchar(8) DEFAULT NULL,"+
							"`SITE_OFFICE_TEL` varchar(8) DEFAULT NULL,"+
							"`SITE_REMARK` varchar(250) DEFAULT NULL);";
			db.execSQL(sql);
		}
	
		protected static void createSiteLocationTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `site_location` ("+
						  	"`LOC_ID` varchar(3) PRIMARY KEY NOT NULL,"+
						  	"`LOC_DISTRICT` varchar(3) NOT NULL,"+
						 	"`LOC_LATITUDE` double NOT NULL,"+
						  	"`LOC_LONGITUDE` double NOT NULL,"+
						  	"`LOC_ADDRESS_CH` varchar(30) NOT NULL,"+
						  	"`LOC_ADDRESS_EN` varchar(160) NOT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createSitePhotoTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `site_photos` ("+
						  	"`SITE_PHOTO_ID` varchar(3) PRIMARY KEY NOT NULL,"+
						  	"`PHOTO_PATH1` varchar(100) DEFAULT NULL,"+
						  	"`PHOTO_PATH2` varchar(100) DEFAULT NULL,"+
						  	"`PHOTO_PATH3` varchar(100) DEFAULT NULL,"+
						  	"`PHOTO_PATH4` varchar(100) DEFAULT NULL,"+
						  	"`PHOTO_PATH5` varchar(100) DEFAULT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createSPIndoorFacTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `sp_facilities_indoor` ("+
						  	"`SP_FAC_IN_ID` varchar(6) PRIMARY KEY NOT NULL,"+
						  	"`MAIN_POOL` int(1) NOT NULL,"+
						  	"`SECONDARY_POOL` int(1) NOT NULL,"+
						  	"`TRANING_POOL` int(1) NOT NULL,"+
						  	"`TEACHING_POOL` int(1) NOT NULL,"+
						  	"`DIVING_POOL` int(1) NOT NULL,"+
						  	"`CHILDREN_POOL` int(1) NOT NULL,"+
						  	"`TODDLER_POOL` int(1) NOT NULL,"+
						  	"`LEISURE_POOL` int(1) NOT NULL,"+
						  	"`SPECTATOR_STAND` int(6) NOT NULL,"+
						  	"`ELEC_SCOREBOARD` int(1) NOT NULL,"+
						  	"`FAMILY_CHG_RM` int(1) NOT NULL,"+
						  	"`JACUZZI` int(1) NOT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createSPOutdoorFacTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `sp_facilities_outdoor` ("+
						  	"`SP_FAC_OUT_ID` varchar(6) PRIMARY KEY NOT NULL,"+
						  	"`MAIN_POOL` int(1) NOT NULL,"+
						  	"`SECONDARY_POOL` int(1) NOT NULL,"+
						  	"`TRANING_POOL` int(1) NOT NULL,"+
						  	"`TEACHING_POOL` int(1) NOT NULL,"+
						  	"`DIVING_POOL` int(1) NOT NULL,"+
						  	"`CHILDREN_POOL` int(1) NOT NULL,"+
						  	"`TODDLER_POOL` int(1) NOT NULL,"+
						  	"`LEISURE_POOL` int(1) NOT NULL,"+
						  	"`SPECTATOR_STAND` int(6) NOT NULL,"+
						  	"`ELEC_SCOREBOARD` int(1) NOT NULL,"+
						  	"`FAMILY_CHG_RM` int(1) NOT NULL,"+
						  	"`JACUZZI` int(1) NOT NULL,"+
						  	"`PARTICIPATORY_FOUNTAIN` int(1) NOT NULL,"+
						  	"`SUN_BATHING_AREA` int(1) NOT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createSPOtherFacTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `sp_facilities_other` ("+
					  		"`SP_FAC_OTHER_ID` varchar(6) PRIMARY KEY NOT NULL,"+
					  		"`DISAB_BFA` bit(1) NOT NULL,"+
					  		"`DISAB_CHG_RM` int(1) NOT NULL,"+
					  		"`DISAB_TOILET` int(1) NOT NULL,"+
					  		"`FAC_PARKING` int(3) NOT NULL,"+
					  		"`FAC_INC_DISAB_PARKING` int(3) NOT NULL);";
			db.execSQL(sql);
		}
	
		protected static void createSPSMrOpenScheduleTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `sp_open_schedule_summer` ("+
						  	"`SP_OS_SM_ID` varchar(6) PRIMARY KEY NOT NULL,"+
						  	"`SP_OS_SM_DESC_CH` varchar(100) NOT NULL,"+
						  	"`SP_OS_SM_DESC_EN` varchar(250) NOT NULL,"+
						  	"`SP_OS_SM_REMARK` varchar(250) DEFAULT NULL);";
			db.execSQL(sql);
		}
			
		protected static void createSPWTOpenScheduleTalbe()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `sp_open_schedule_winter` ("+
						  	"`SP_OS_WT_ID` varchar(6) PRIMARY KEY NOT NULL,"+
						  	"`SP_OS_WT_DESC_CH` varchar(100) NOT NULL,"+
						  	"`SP_OS_WT_DESC_EN` varchar(250) NOT NULL,"+
						  	"`SP_OS_WT_REMARK` varchar(250) DEFAULT NULL);";
			db.execSQL(sql);
		}
	
		protected static void createSPOtherInfoTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `sp_other_info` ("+
						  	"`SP_ID` varchar(4) NOT NULL DEFAULT 'SP',"+
						  	"`SP_FAC_INDOOR_ID` varchar(6) DEFAULT NULL,"+
						  	"`SP_FAC_OUTDOOR_ID` varchar(6) DEFAULT NULL,"+
						  	"`SP_FAC_OTHER_ID` varchar(6) DEFAULT NULL,"+
						  	"`SP_OPEN_SCHEDULE_SUMMER_ID` varchar(6) DEFAULT NULL,"+
						  	"`SP_OPEN_SCHEDULE_WINTER_ID` varchar(6) DEFAULT NULL,"+
						  	"`SP_WEEKLY_CLEANS_OPEAR` varchar(3) NOT NULL,"+
						  	"`SP_HOLIDAY_CLEANS_OPEAR` varchar(3) NOT NULL,"+
						  	"`SP_MAINTENANCE_DESC_CH` varchar(100) NOT NULL,"+
						  	"`SP_MAINTENANCE_DESC_EN` varchar(250) NOT NULL,"+
						  	"`SP_MAINTENANCE_REMARK` varchar(250) DEFAULT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createWeekTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `week` ("+
						  	"`WEEK_ID` varchar(3) PRIMARY KEY NOT NULL,"+
						  	"`WEEK_DESC_CH` varchar(3)  NOT NULL,"+
						  	"`WEEK_DESC_EN` varchar(12) NOT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createUpdateInfoTable()
		{
			String sql =	"CREATE TABLE IF NOT EXISTS `update_info` ("+
						  	"`table_name` varchar(50) PRIMARY KEY NOT NULL,"+
						  	"`comment` varchar(200) NOT NULL,"+
						  	"`last_update` int(9) NOT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createInfoNomment()
		{
			String sql = "CREATE TABLE IF NOT EXISTS `info_comment` ("+
					     "`INFO_COMMENT_ID` varchar(5) PRIMARY KEY NOT NULL,"+
					     "`INFO_COMMENT_DESC_CH` varchar(50) DEFAULT NULL,"+
					     "`INFO_COMMENT_DESC_EN` varchar(150) DEFAULT NULL,"+
					     "`INFO_COMMENT_REMARK` varchar(250) DEFAULT NULL);";
			db.execSQL(sql);
		}
		
		protected static void createInfoRelationship()
		{
			String sql = "CREATE TABLE IF NOT EXISTS `info_relationship` ("+
						 "`TABLE_COLUMN_ID` varchar(6) NOT NULL,"+
  						 "`TABLE_COLUMN_COMMENT_ID` varchar(4) NOT NULL);";
			db.execSQL(sql);
		}
	}
	
	protected static class Insert
	{		
		protected static void insertBeachIndoorFacTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO bh_facilities_indoor values " +
							"('" + jsondata.getString("BH_FAC_IN_ID") + "'," +
							"" + jsondata.getString("ACTIVITY_RM") + ");";
			db.execSQL(sql);
		}
		protected static void insertBHOutdoorFacTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO bh_facilities_outdoor values "
							+ "('"+jsondata.getString("BH_FAC_OUT_ID")+"'," +
							""+jsondata.getString("FAST_FOOD_KIOSK")+"," +
							""+jsondata.getString("LIGHT_REFRESHMENT_KIOSK")+"," +
							""+jsondata.getString("RESTAURANT")+"," +
							""+jsondata.getString("BBQ_PIT")+"," +
							""+jsondata.getString("CHG_RM")+"," +
							""+jsondata.getString("SHOWER_FAC")+"," +
							""+jsondata.getString("TOILET")+"," +
							""+jsondata.getString("BATHING_SHED")+"," +
							""+jsondata.getString("RAFT")+"," +
							""+jsondata.getString("BH_VB_COURT")+"," +
							""+jsondata.getString("PLAYGROUND")+"," +
							""+jsondata.getString("GOLF_COURSE")+"," +
							""+jsondata.getString("FITNESS_EQUIP_FOR_ELDERLY")+"," +
							""+jsondata.getString("FEE_CHARGING_CAR_PARK")+"," +
							""+jsondata.getString("FAC_INC_DISAB_PARKING")+");";
			db.execSQL(sql);
		}
		
		protected static void insertBHOtherInfoTable(JSONObject jsondata) throws JSONException
		{
			String sql = 	"INSERT INTO bh_other_info values "
							+ "('"+jsondata.getString("BH_ID")+"'," +
							"'"+jsondata.getString("BH_FAC_INDOOR_ID")+"'," +
							"'"+jsondata.getString("BH_FAC_OUTDOOR_ID")+"'," +
							"'"+jsondata.getString("BH_LG_SERV_TIME_SM_ID")+"'," +
							"'"+jsondata.getString("BH_LG_SERV_TIME_WT_ID")+"',"+
							"'"+jsondata.getString("IS_PUBLIC")+"');";
			db.execSQL(sql);
			Log.d(TAG, "sql: "+sql);
		}
		
		protected static void insertBHLGServSMTimeTable(JSONObject jsondata) throws JSONException
		{
			String sql 	= 	"INSERT INTO bh_lifeguard_service_time_summer values " +
							"('" + jsondata.getString("BH_LG_ST_SM_ID") + "'," +
							"'" + jsondata.getString("BH_LG_ST_SM_DESC_CH") + "'," +
							"'" + jsondata.getString("BH_LG_ST_SM_DESC_EN") + "'," +
							"'" + jsondata.getString("BH_LG_ST_SM_REMARK") + "');";
			db.execSQL(sql);			
		}
		
		protected static void insertBHLGServWTTimeTable(JSONObject jsondata) throws JSONException
		{
			String sql = 	"INSERT INTO bh_lifeguard_service_time_winter values " +
							"('"+jsondata.getString("BH_LG_ST_WT_ID")+"'," +
							"'"+jsondata.getString("BH_LG_ST_WT_DESC_CH")+"'," +
							"'"+jsondata.getString("BH_LG_ST_WT_DESC_EN")+"'," +
							"'"+jsondata.getString("BH_LG_ST_WT_REMARK")+"');";
			db.execSQL(sql);			
		}
		
		protected static void insertHKDistrictTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO hk_district values "
							+ "('"+jsondata.getString("DISTRICT_ID")+"'," +
							"'"+jsondata.getString("REGION")+"'," +
							"'"+jsondata.getString("DISTRICT_DESC_CH")+"'," +
							"'"+jsondata.getString("DISTRICT_DESC_EN")+"');";
						
			db.execSQL(sql);	
		}
		
		protected static void insertHKRegionTable(JSONObject jsondata) throws JSONException
		{
			String sql = 	"INSERT INTO hk_region values "
							+ "('"+jsondata.getString("REGION_ID")+"'," +
							"'"+jsondata.getString("REGION_DESC_CH")+"'," +
							"'"+jsondata.getString("REGION_DESC_EN")+"');";
						
			db.execSQL(sql);
		}
		
		protected static void insertSiteTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO site values "
							+ "('"+jsondata.getString("SITE_ID")+"'," +
							"'"+jsondata.getString("SITE_TYPE")+"'," +
							"'"+jsondata.getString("SITE_LOC_ID")+"'," +
							"'"+jsondata.getString("SITE_PHOTO_ID")+"'," +
							"'"+jsondata.getString("SITE_NAME_CH")+"'," +
							"'"+jsondata.getString("SITE_NAME_EN")+"'," +
							"'"+jsondata.getString("SITE_TEL")+"'," +
							"'"+jsondata.getString("SITE_OFFICE_TEL")+"'," +
							"'"+jsondata.getString("SITE_REMARK")+"');";
			db.execSQL(sql);
		}
	
		protected static void insertSiteLocationTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO site_location values "
							+ "('"+jsondata.getString("LOC_ID")+"'," +
							"'"+jsondata.getString("LOC_DISTRICT")+"'," +
							""+jsondata.getString("LOC_LATITUDE")+"," +
							""+jsondata.getString("LOC_LONGITUDE")+"," +
							"'"+jsondata.getString("LOC_ADDRESS_CH")+"'," +
							"'"+jsondata.getString("LOC_ADDRESS_EN")+"');";
			db.execSQL(sql);
		}
		
		protected static void insertSitePhotoTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO site_photos values "
							+ "('"+jsondata.getString("SITE_PHOTO_ID")+"'," +
							"'"+jsondata.getString("PHOTO_PATH1")+"'," +
							"'"+jsondata.getString("PHOTO_PATH2")+"'," +
							"'"+jsondata.getString("PHOTO_PATH3")+"'," +
							"'"+jsondata.getString("PHOTO_PATH4")+"'," +
							"'"+jsondata.getString("PHOTO_PATH5")+"');";	
			db.execSQL(sql);
		}
		
		protected static void insertSPIndoorFacTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO sp_facilities_indoor values "
							+ "('"+jsondata.getString("SP_FAC_IN_ID")+"'," +
							""+jsondata.getString("MAIN_POOL")+"," +
							""+jsondata.getString("SECONDARY_POOL")+"," +
							""+jsondata.getString("TRANING_POOL")+"," +
							""+jsondata.getString("TEACHING_POOL")+"," +
							""+jsondata.getString("DIVING_POOL")+"," +
							""+jsondata.getString("CHILDREN_POOL")+"," +
							""+jsondata.getString("TODDLER_POOL")+"," +
							""+jsondata.getString("LEISURE_POOL")+"," +
							""+jsondata.getString("SPECTATOR_STAND")+"," +
							""+jsondata.getString("ELEC_SCOREBOARD")+"," +
							""+jsondata.getString("FAMILY_CHG_RM")+"," +
							""+jsondata.getString("JACUZZI")+");";
							//""+jsondata.getString("PARTICIPATORY_FOUNTAIN")+");";
			db.execSQL(sql);
		}
		
		protected static void insertSPOutdoorFacTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO sp_facilities_outdoor values "
							+ "('"+jsondata.getString("SP_FAC_OUT_ID")+"'," +
							""+jsondata.getString("MAIN_POOL")+"," +
							""+jsondata.getString("SECONDARY_POOL")+"," +
							""+jsondata.getString("TRANING_POOL")+"," +
							""+jsondata.getString("TEACHING_POOL")+"," +
							""+jsondata.getString("DIVING_POOL")+"," +
							""+jsondata.getString("CHILDREN_POOL")+"," +
							""+jsondata.getString("TODDLER_POOL")+"," +
							""+jsondata.getString("LEISURE_POOL")+"," +
							""+jsondata.getString("SPECTATOR_STAND")+"," +
							""+jsondata.getString("ELEC_SCOREBOARD")+"," +
							""+jsondata.getString("FAMILY_CHG_RM")+"," +
							""+jsondata.getString("JACUZZI")+"," +
							""+jsondata.getString("PARTICIPATORY_FOUNTAIN")+"," +
							""+jsondata.getString("SUN_BATHING_AREA")+");";
			db.execSQL(sql);
		}
		
		protected static void insertSPOtherFacTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO sp_facilities_other values "
							+ "('"+jsondata.getString("SP_FAC_OTHER_ID")+"'," +
							""+jsondata.getString("DISAB_BFA")+"," +
							""+jsondata.getString("DISAB_CHG_RM")+"," +
							""+jsondata.getString("DISAB_TOILET")+"," +
							""+jsondata.getString("FAC_PARKING")+"," +
							""+jsondata.getString("FAC_INC_DISAB_PARKING")+");";		
			db.execSQL(sql);
		}
	
		protected static void insertSPSMrOpenScheduleTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO sp_open_schedule_summer values "
							+ "('"+jsondata.getString("SP_OS_SM_ID")+"'," +
							"'"+jsondata.getString("SP_OS_SM_DESC_CH")+"'," +
							"'"+jsondata.getString("SP_OS_SM_DESC_EN")+"'," +
							"'"+jsondata.getString("SP_OS_SM_REMARK")+"');";
			db.execSQL(sql);
		}
			
		protected static void insertSPWTOpenScheduleTalbe(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO sp_open_schedule_winter values "
							+ "('"+jsondata.getString("SP_OS_WT_ID")+"'," +
							"'"+jsondata.getString("SP_OS_WT_DESC_CH")+"'," +
							"'"+jsondata.getString("SP_OS_WT_DESC_EN")+"'," +
							"'"+jsondata.getString("SP_OS_WT_REMARK")+"');";
			db.execSQL(sql);
		}
	
		protected static void insertSPOtherInfoTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO sp_other_info values "
							+ "('"+jsondata.getString("SP_ID")+"'," +
							"'"+jsondata.getString("SP_FAC_INDOOR_ID")+"'," +
							"'"+jsondata.getString("SP_FAC_OUTDOOR_ID")+"'," +
							"'"+jsondata.getString("SP_FAC_OTHER_ID")+"'," +
							"'"+jsondata.getString("SP_OPEN_SCHEDULE_SUMMER_ID")+"'," +
							"'"+jsondata.getString("SP_OPEN_SCHEDULE_WINTER_ID")+"'," +
							"'"+jsondata.getString("SP_WEEKLY_CLEANS_OPEAR")+"'," +
							"'"+jsondata.getString("SP_HOLIDAY_CLEANS_OPEAR")+"'," +
							"'"+jsondata.getString("SP_MAINTENANCE_DESC_CH")+"'," +
							"'"+jsondata.getString("SP_MAINTENANCE_DESC_EN")+"'," +
							"'"+jsondata.getString("SP_MAINTENANCE_REMARK")+"');";
			db.execSQL(sql);
		}
		
		protected static void insertWeekTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO week values "
							+ "('"+jsondata.getString("WEEK_ID")+"'," +
							"'"+jsondata.getString("WEEK_DESC_CH")+"'," +
							"'"+jsondata.getString("WEEK_DESC_EN")+"');";
			db.execSQL(sql);
		}
		
		protected static void insertInfoComment(JSONObject jsondata) throws JSONException
		{
			String sql = "INSERT INTO info_comment values"+
					     "('"+jsondata.getString("INFO_COMMENT_ID")+"'," +
					     "'"+jsondata.getString("INFO_COMMENT_DESC_CH")+"'," +
					     "'"+jsondata.getString("INFO_COMMENT_DESC_EN")+"'," +
					     "'"+jsondata.getString("INFO_COMMENT_REMARK")+"');";
			db.execSQL(sql);
		}
		
		protected static void insertUpdateInfoTable(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO update_info values "
							+ "('"+jsondata.getString("table_name")+"'," +
							"'"+jsondata.getString("comment")+"'," +
							""+jsondata.getString("last_update")+");";		
			db.execSQL(sql);
		}
		
		protected static void insertInfoRelationship(JSONObject jsondata) throws JSONException
		{
			String sql =	"INSERT INTO info_relationship values "
							+ "('"+jsondata.getString("TABLE_COLUMN_ID")+"'," +
							"'"+jsondata.getString("TABLE_COLUMN_COMMENT_ID")+"');";		
			db.execSQL(sql);
		}
		
	}
	
	protected static class Search
	{
		protected static int searchSPABUpdateTime(String tablename)
		{
			int lastupdate=0;
			String sql = "Select * FROM update_info WHERE table_name='" + tablename+ "';";
			Cursor cursor = db.rawQuery(sql, null);
			if( cursor == null || cursor.getCount() == 0){
				Log.e(TAG, "cursor == null!");
				return -1;
			}
			while (cursor.moveToNext()) 
			{
				lastupdate = cursor.getInt(cursor.getColumnIndex("last_update"));
			}
			return lastupdate;	
		}
		
		protected static Cursor getALLSPABRegion()
		{
			String sql = "SELECT * FROM hk_region ;";
			return db.rawQuery(sql, null);
		}
	}
	
	protected static class Update
	{
		protected static void updateSPABTable(JSONObject jsondata)
		{
			try 
			{
				String sql =	"UPDATE update_info SET last_update="+jsondata.getInt("last_update")+
								" WHERE table_name='"+jsondata.getString("table_name")+"';";
				db.execSQL(sql);	
			} 
			catch (SQLException e) 
			{
				Log.e(TAG, "updateSPABTable:" + e.getMessage());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	protected static class Drop
	{
		protected static void dropSPABTable(JSONObject jsondata)
		{
			try 
			{
				String sql = "DROP TABLE IF EXISTS "+ jsondata.getString("table_name");
				db.execSQL(sql);
			} 
			catch (JSONException e) 
			{
				e.printStackTrace();
			}
		}
	}
}