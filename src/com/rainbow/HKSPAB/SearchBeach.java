package com.rainbow.HKSPAB;

import java.util.Locale;

import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class SearchBeach extends Activity implements OnItemSelectedListener, TextWatcher, OnCheckedChangeListener, OnItemClickListener {
	static Context context;
	private static final String TAG = SearchBeach.class.getName();
	private ListView lstResult;
	private SharedPreferences splanguage;
	private Spinner sprDistrict, sprRegion;
	private String language ="EN";
	private EditText etName;
	private String [] siteid , sitename , regionname , regionid , districtname , districtid;
	private String mPath = "";
	private AdView adView;
	private FuncGenerator generator;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPath = getString(R.string.database_path)+"/"+getString(R.string.database_name);
		initDataValue();
		initConfigLocale();
		initFunction();
		initEventHandler();
        LinearLayout layout = (LinearLayout) findViewById(R.id.mainLayout);
        generator.setAdmob(adView, layout, context);
	}
	
	private void initFunction() 
	{
		generator.getAnalysis("SearchBeach");
		//generator.getAdvertisement(SearchSwim.this);
		selectSiteTable("BH","");
		selectRegionTable();
	}
	
	private void selectSiteTable(String sitetype, String where) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.CREATE_IF_NECESSARY);	
		String sql = "SELECT COUNT(*) FROM site , site_location , hk_district WHERE SITE_LOC_ID = LOC_ID AND LOC_DISTRICT = DISTRICT_ID AND SITE_TYPE = '"+sitetype+"' "+ where+";";
		if(db == null){
			Log.e(TAG, "No db find!");
			return;
		}
		Cursor cursor = db.rawQuery(sql, null);
		if( cursor == null || cursor.getCount() == 0){
			Log.e(TAG, "cursor == null!");
			return;
		}
		cursor.moveToFirst();
		int count= cursor.getInt(0);
		//Log.e("count", count+"");
		siteid = new String[count];
		sitename = new String[count];
		sql = "SELECT * FROM site , site_location , hk_district WHERE SITE_LOC_ID = LOC_ID AND LOC_DISTRICT = DISTRICT_ID AND SITE_TYPE = '"+sitetype+"' "+ where+" ;";
		cursor = db.rawQuery(sql, null);
		int countnum = 0;
		while (cursor.moveToNext()) 
		{
			siteid[countnum] = cursor.getString(cursor.getColumnIndex("SITE_ID"));
			sitename[countnum] = cursor.getString(cursor.getColumnIndex("SITE_NAME_"+language));
			//Log.e("cursor", siteid[countnum] + " " + sitename[countnum]);
			++countnum;
		}
		addItemToListView();
	}
	
	private void addItemToListView() 
	{
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this , android.R.layout.simple_list_item_1 , sitename);
		lstResult.setAdapter(arrayAdapter);	
	}
	
	public void onItemSelected(AdapterView<?> parent, View arg1, int count, long arg3) 
	{
		switch(parent.getId()) 
		{
			case R.id.sprRegion:
				SelectDistrictTable(count);
				break;
			case R.id.sprArea:
				updateWhere();
				break;
		}	
	}
	
	private void updateWhere() 
	{
		String where = "AND SITE_NAME_"+language+" LIKE '%"+etName.getText().toString().replaceAll("'", "")+"%'";
		int regionmun =  sprRegion.getSelectedItemPosition();
		int districtmun = sprDistrict.getSelectedItemPosition();
		if(regionmun==0)
		{
			if(districtmun!=0) where+=" AND LOC_DISTRICT = '"+districtid[districtmun]+"' "; 
		}
		else
		{
			if(districtmun==0) where+= " AND REGION='"+regionid[regionmun]+"' ";
			else where+=" AND LOC_DISTRICT = '"+districtid[districtmun]+"' ";
		}

		
		//Log.e("where", where);
		selectSiteTable("BH",where);
	}
	
	public void onTextChanged(CharSequence s, int start, int before, int count) 
	{
		updateWhere();
	}
	
	private void SelectDistrictTable(int selectid) 
	{
		String sqlwhere ="";
		if(selectid==0)	sqlwhere ="";
		else sqlwhere = "WHERE REGION = '"+regionid[selectid]+"'";
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.CREATE_IF_NECESSARY);	
		String sql = "SELECT COUNT(*) FROM hk_district "+sqlwhere+";";
		if(db == null){
			Log.e(TAG, "No db find!");
			return;
		}
		Cursor cursor = db.rawQuery(sql, null);
		if(cursor.getColumnCount() == 0){
			Log.e(TAG, "No data find!");
			return;
		}
		cursor.moveToFirst();
		int count= cursor.getInt(0);
		districtname = new String[(count+1)];
		districtid = new String[(count+1)];
		districtname[0] = getString(R.string.district_all_name);
		districtid[0] = "ALL";
		sql = "SELECT * FROM hk_district "+sqlwhere+";";
		cursor = db.rawQuery(sql, null);
		int countnum = 1;
		while (cursor.moveToNext()) 
		{
			districtid[countnum] = cursor.getString(cursor.getColumnIndex("DISTRICT_ID"));
			districtname[countnum] = cursor.getString(cursor.getColumnIndex("DISTRICT_DESC_"+language));
			++countnum;
		}
		addItemToDistrictSpinner();
	}
	
	private void selectRegionTable() 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.CREATE_IF_NECESSARY);
		String sql = "SELECT COUNT(*) FROM hk_region ;";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToFirst();
		int count= cursor.getInt(0);
		sql = "SELECT * FROM hk_region ;";
		cursor = db.rawQuery(sql, null);
		int countnum = 1;
		regionname = new String[(count+1)];
		regionid = new String[(count+1)];
		regionname[0] = getString(R.string.region_all_name);
		regionid[0] = "ALL";
		while (cursor.moveToNext())
		{
			regionname[countnum] = cursor.getString(cursor.getColumnIndex("REGION_DESC_"+language));
			regionid[countnum] = cursor.getString(cursor.getColumnIndex("REGION_ID"));
			++countnum;
		}
		addItemToRegionSpinner();
	}
	
	private void addItemToRegionSpinner() 
	{
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this , android.R.layout.simple_spinner_item , regionname);
		sprRegion.setAdapter(arrayAdapter);
	}
	
	private void initEventHandler()
	{
		etName.addTextChangedListener(this);
		sprDistrict.setOnItemSelectedListener(this);
		sprRegion.setOnItemSelectedListener(this);
		lstResult.setOnItemClickListener(this);
		//rgInOut.setOnCheckedChangeListener(this);
	}
	
	private void addItemToDistrictSpinner() 
	{
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this , android.R.layout.simple_spinner_item , districtname);
		sprDistrict.setAdapter(arrayAdapter);	
	}
	
	private void initDataValue() 
	{
		context = SearchBeach.this;
		generator = new FuncGenerator();  
		splanguage = context.getSharedPreferences("LANGUAGE", MODE_PRIVATE);
		initConfigLocale();
		setContentView(R.layout.search_beach);
		lstResult = (ListView)findViewById(R.id.lstResult);
		sprDistrict = (Spinner)findViewById(R.id.sprArea);
		sprRegion = (Spinner)findViewById(R.id.sprRegion);
		etName = (EditText)findViewById(R.id.edtName);
	}
	
	private void initConfigLocale()
	{
		Configuration config = new Configuration(getResources().getConfiguration());
        if(splanguage.getInt("LANGUAGE_ID", -1)==1){
        	config.locale = Locale.ENGLISH;
        	language = "EN";
        }
        else if(splanguage.getInt("LANGUAGE_ID", -1)==2){
        	config.locale = Locale.CHINESE;
        	language = "CH";
        } else{
        	if(Locale.getDefault().getLanguage().equals("zh")){
        		config.locale = Locale.CHINESE;
        		language = "CH"; 
        	}
        }
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
	
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    	Intent intent = new Intent(SearchBeach.this, beachDetail.class);
    	intent.putExtra("siteid", siteid[position]);
		startActivity(intent);
		//Log.e("where", "r");
	}
}
