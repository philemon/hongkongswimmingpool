package com.rainbow.HKSPAB;

import java.io.InputStream;
import java.util.Locale;

import com.rainbow.HKSPAB.swimDetail.DownloadImageTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class beachDetail extends Activity implements OnClickListener{
	private static final String TAG = swimDetail.class.getName();
	static Context context;
	private FuncGenerator generator;
	private SharedPreferences splanguage;
	private String language ="EN";
	private String mPath = "";
	private String siteID = "";
	private LinearLayout llSiteInfo, llInfo;
	private String latitude = "";
	private String longitude = "";
	//RelativeLayout rlGc;
	private ScrollView svInfo;
	private LinearLayout ll;
	private int mCount;
	private String[] mImageUrl;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (savedInstanceState == null) {
			Bundle extras = getIntent().getExtras();
		    if(extras == null) {
		    	siteID = null;
		    } else {
		    	siteID = extras.getString("siteid");
		    }
		}
		generator = new FuncGenerator();  
		mPath = getString(R.string.database_path)+"/"+getString(R.string.database_name);
		initDataValue();
		initConfigLocale();
		initFunction();
	}
	
	
	private void initFunction() 
	{
		//generator.getAnalysis("SearchSwim");
		//generator.getAdvertisement(SearchSwim.this);
		getSPInfo(siteID);
	}
	
	private void initDataValue() 
	{
		context = beachDetail.this;
		//generator = new FuncGenerator();  
		splanguage = context.getSharedPreferences("LANGUAGE", MODE_PRIVATE);
		//setContentView(R.layout.details_swim);
	}
	
	private void initConfigLocale()
	{
		Configuration config = new Configuration(getResources().getConfiguration());
        if(splanguage.getInt("LANGUAGE_ID", -1)==1){
        	config.locale = Locale.ENGLISH;
        	language = "EN";
        }
        else if(splanguage.getInt("LANGUAGE_ID", -1)==2){
        	config.locale = Locale.CHINESE;
        	language = "CH";
        }else{
        	if(Locale.getDefault().getLanguage().equals("zh")){
        		config.locale = Locale.CHINESE;
        		language = "CH"; 
        	}
        }
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
	}
	
	private void getSPInfo(String siteID) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);	
		
		String sql = "SELECT * FROM hk_district, hk_region, site, site_location, bh_other_info WHERE ";
		sql += "site.site_id = bh_other_info.bh_id AND site.site_loc_id = site_location.loc_id AND ";
		sql += "site_location.loc_district = hk_district.district_id AND hk_district.region = hk_region.region_id AND ";
		sql += "site.site_id = '" + siteID + "';";
		
		//
		//
		
		/*String sql = "SELECT * FROM  ,  ,  ,  , site_photos , ";
		sql += "sp_facilities_indoor , sp_facilities_other , sp_facilities_outdoor ,  , ";
		sql += "sp_open_schedule_winter , ";
		sql += " WHERE site.site_id = sp_other_info.sp_id AND site.site_loc_id = site_location.loc_id AND site.site_photo_id = site_photos.site_photo_id";
		sql += " AND site_location.loc_district = hk_district.district_id AND hk_district.region = hk_region.region_id";
		sql += " AND sp_facilities_indoor.sp_fac_in_id = sp_other_info.sp_fac_indoor_id AND ";
		sql += "sp_facilities_outdoor.sp_fac_out_id = sp_other_info.sp_fac_outdoor_id AND ";
		sql += "sp_facilities_other.sp_fac_other_id = sp_other_info.sp_fac_other_id AND ";
		sql += "sp_open_schedule_summer.sp_os_sm_id = sp_other_info.sp_open_schedule_summer_id AND ";
		sql += "sp_open_schedule_winter.sp_os_wt_id = sp_other_info.sp_open_schedule_winter_id AND site.site_id = '" + siteID + "';";
		*/
		//sql = "SELECT * FROM site;";
		
		Log.d("getSPInfo sql", sql);
		
		//Cursor cursor = db.rawQuery(sql, null);
		//cursor.moveToFirst();
		//int count= cursor.getInt(0);
		//Log.e("count", count+"");
		//siteid = new String[count];
		//sitename = new String[count];
		//sql = "SELECT * FROM site , site_location , hk_district , sp_other_info WHERE SP_ID = SITE_ID AND SITE_LOC_ID = LOC_ID AND LOC_DISTRICT = DISTRICT_ID AND SITE_TYPE = '"+sitetype+"' "+ where+" ;";
		Cursor cursor = db.rawQuery(sql, null);
		//int countnum = 0;
		cursor.moveToNext();
		siteInfo(cursor.getString(cursor.getColumnIndex("SITE_NAME_" + "EN")), 
				cursor.getString(cursor.getColumnIndex("SITE_NAME_" + language)), 
				cursor.getString(cursor.getColumnIndex("DISTRICT_DESC_" + language)), 
				cursor.getString(cursor.getColumnIndex("REGION_DESC_" + language)), 
				cursor.getString(cursor.getColumnIndex("LOC_ADDRESS_" + language)), 
				cursor.getString(cursor.getColumnIndex("SITE_TEL")), 
				cursor.getString(cursor.getColumnIndex("SITE_OFFICE_TEL")), 
				cursor.getString(cursor.getColumnIndex("IS_PUBLIC")), 
				cursor.getString(cursor.getColumnIndex("BH_FAC_INDOOR_ID")), 
				cursor.getString(cursor.getColumnIndex("BH_FAC_OUTDOOR_ID")), 
				cursor.getString(cursor.getColumnIndex("BH_LG_SERV_TIME_SM_ID")), 
				cursor.getString(cursor.getColumnIndex("BH_LG_SERV_TIME_WT_ID")));

		latitude = cursor.getString(cursor.getColumnIndex("LOC_LATITUDE"));
		longitude = cursor.getString(cursor.getColumnIndex("LOC_LONGITUDE"));
			//Log.e("BID", cursor.getString(cursor.getColumnIndex("SITE_NAME_" + language)));
			//Log.e("siteid", cursor.getString(cursor.getColumnIndex("SITE_ID")));
			//siteid[countnum] = cursor.getString(cursor.getColumnIndex("SITE_ID"));
			//sitename[countnum] = cursor.getString(cursor.getColumnIndex("SITE_NAME_"+language));
			//Log.e("cursor", siteid[countnum] + " " + sitename[countnum]);
			//++countnum;
	}
	
	private String chkSumSch(String sumID) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);	
		
		String sql = "SELECT * FROM BH_LIFEGUARD_SERVICE_TIME_SUMMER WHERE BH_LG_ST_SM_ID = '" + sumID + "'";
		//Log.e("sql", sql);
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		return cursor.getString(cursor.getColumnIndex("BH_LG_ST_SM_DESC_" + language));

	}
	
	public void sumInfo(String sumDesc){
		TextView tvSum = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvSum.setText(getString(R.string.sum_sch) + ": " + sumDesc);
		tvSum.setTextSize(18);
		tvSum.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvSum.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvSum.setBackgroundResource(R.color.white);
		tvSum.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvSum);

		View line = new View(context); line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
	}
	
	private String chkWinSch(String winID) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);	
		
		String sql = "SELECT * FROM BH_LIFEGUARD_SERVICE_TIME_WINTER WHERE BH_LG_ST_WT_ID = '" + winID + "'";
		//Log.e("sql", sql);
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		return cursor.getString(cursor.getColumnIndex("BH_LG_ST_WT_DESC_" + language));

	}
	
	public void winInfo(String winDesc){
		TextView tvWin = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvWin.setText(getString(R.string.win_sch) + ": " + winDesc);
		tvWin.setTextSize(18);
		tvWin.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvWin.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvWin.setBackgroundResource(R.color.white);
		tvWin.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvWin);

		View line = new View(context); line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
	}
	
	private String chkIndoorF(String indoorFID) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);	
		
		String sql = "SELECT * FROM BH_FACILITIES_INDOOR WHERE BH_FAC_IN_ID = '" + indoorFID + "'";
		//Log.e("sql", sql);
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		return cursor.getString(cursor.getColumnIndex("ACTIVITY_RM"));

	}
	
	public void indoorFInfo(String actRm){
		TextView tvActRm = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvActRm.setText(getString(R.string.act_rm) + ": " + actRm);
		tvActRm.setTextSize(18);
		tvActRm.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvActRm.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvActRm.setBackgroundResource(R.color.white);
		tvActRm.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvActRm);

		View line = new View(context); line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
	}
	
	private void chkOutdoor(String outID) 
	{
		SQLiteDatabase db = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);	
		
		String sql = "SELECT * FROM BH_FACILITIES_OUTDOOR WHERE BH_FAC_OUT_ID = '" + outID + "'";
		//Log.e("sql", sql);
		Cursor cursor = db.rawQuery(sql, null);
		if( cursor == null || cursor.getCount() == 0){
			Log.e(TAG, "cursor == null!");
			return;
		}
		cursor.moveToNext();
		
		outInfo(cursor.getString(cursor.getColumnIndex("FAST_FOOD_KIOSK")), 
				cursor.getString(cursor.getColumnIndex("LIGHT_REFRESHMENT_KIOSK")), 
				cursor.getString(cursor.getColumnIndex("RESTAURANT")), 
				cursor.getString(cursor.getColumnIndex("BBQ_PIT")), 
				cursor.getString(cursor.getColumnIndex("CHG_RM")), 
				cursor.getString(cursor.getColumnIndex("SHOWER_FAC")), 
				cursor.getString(cursor.getColumnIndex("TOILET")), 
				cursor.getString(cursor.getColumnIndex("BATHING_SHED")), 
				cursor.getString(cursor.getColumnIndex("RAFT")), 
				cursor.getString(cursor.getColumnIndex("BH_VB_COURT")), 
				cursor.getString(cursor.getColumnIndex("PLAYGROUND")), 
				cursor.getString(cursor.getColumnIndex("GOLF_COURSE")), 
				cursor.getString(cursor.getColumnIndex("FITNESS_EQUIP_FOR_ELDERLY")), 
				cursor.getString(cursor.getColumnIndex("FEE_CHARGING_CAR_PARK")), 
				cursor.getString(cursor.getColumnIndex("FAC_INC_DISAB_PARKING")));
		
		/*siteInfo(cursor.getString(cursor.getColumnIndex("SITE_NAME_" + language)), 
		cursor.getString(cursor.getColumnIndex("DISTRICT_DESC_" + language)), 
		cursor.getString(cursor.getColumnIndex("REGION_DESC_" + language)), 
		cursor.getString(cursor.getColumnIndex("LOC_ADDRESS_" + language)), 
		cursor.getString(cursor.getColumnIndex("SITE_TEL")), 
		cursor.getString(cursor.getColumnIndex("SITE_OFFICE_TEL")), 
		cursor.getString(cursor.getColumnIndex("SP_WEEKLY_CLEANS_OPEAR")), 
		cursor.getString(cursor.getColumnIndex("SP_HOLIDAY_CLEANS_OPEAR")), 
		cursor.getString(cursor.getColumnIndex("SP_MAINTENANCE_DESC_" + language)), 
		cursor.getString(cursor.getColumnIndex("SP_OPEN_SCHEDULE_SUMMER_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_OPEN_SCHEDULE_WINTER_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_FAC_INDOOR_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_FAC_OUTDOOR_ID")), 
		cursor.getString(cursor.getColumnIndex("SP_FAC_OTHER_ID")));*/

	}
	
	public void outInfo(String fastFood, String lightFood, String restaurant, String bbqPit, String chgRm, String ShowFac, 
			String toilet, String bathShed, String raft, String vbCourt, String playG, String golfC, String fitEq, String feeCarPark, String disabCarPark){
		TextView tvFastFood = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvFastFood.setText(getString(R.string.fast_food) + ": " + fastFood);
		tvFastFood.setTextSize(18);
		tvFastFood.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvFastFood.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvFastFood.setBackgroundResource(R.color.white);
		tvFastFood.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvFastFood);

		View line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvLightFood = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvLightFood.setText(getString(R.string.light_food) + ": " + lightFood);
		tvLightFood.setTextSize(18);
		tvLightFood.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvLightFood.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvLightFood.setBackgroundResource(R.color.white);
		tvLightFood.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvLightFood);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvRestaurant = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvRestaurant.setText(getString(R.string.restaurant) + ": " + restaurant);
		tvRestaurant.setTextSize(18);
		tvRestaurant.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvRestaurant.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvRestaurant.setBackgroundResource(R.color.white);
		tvRestaurant.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvRestaurant);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvBbqPit = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvBbqPit.setText(getString(R.string.bbq_pit) + ": " + bbqPit);
		tvBbqPit.setTextSize(18);
		tvBbqPit.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvBbqPit.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvBbqPit.setBackgroundResource(R.color.white);
		tvBbqPit.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvBbqPit);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvChgRm = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvChgRm.setText(getString(R.string.chg_rm) + ": " + chgRm);
		tvChgRm.setTextSize(18);
		tvChgRm.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvChgRm.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvChgRm.setBackgroundResource(R.color.white);
		tvChgRm.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvChgRm);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvShowFac = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvShowFac.setText(getString(R.string.show_fac) + ": " + ShowFac);
		tvShowFac.setTextSize(18);
		tvShowFac.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvShowFac.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvShowFac.setBackgroundResource(R.color.white);
		tvShowFac.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvShowFac);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvToilet = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvToilet.setText(getString(R.string.toilet) + ": " + toilet);
		tvToilet.setTextSize(18);
		tvToilet.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvToilet.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvToilet.setBackgroundResource(R.color.white);
		tvToilet.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvToilet);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvBathShed = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvBathShed.setText(getString(R.string.bath_shed) + ": " + bathShed);
		tvBathShed.setTextSize(18);
		tvBathShed.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvBathShed.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvBathShed.setBackgroundResource(R.color.white);
		tvBathShed.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvBathShed);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvRaft = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvRaft.setText(getString(R.string.raft) + ": " + raft);
		tvRaft.setTextSize(18);
		tvRaft.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvRaft.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvRaft.setBackgroundResource(R.color.white);
		tvRaft.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvRaft);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvVbCourt = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvVbCourt.setText(getString(R.string.vb_court) + ": " + vbCourt);
		tvVbCourt.setTextSize(18);
		tvVbCourt.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvVbCourt.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvVbCourt.setBackgroundResource(R.color.white);
		tvVbCourt.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvVbCourt);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvPlayG = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvPlayG.setText(getString(R.string.play_g) + ": " + playG);
		tvPlayG.setTextSize(18);
		tvPlayG.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvPlayG.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvPlayG.setBackgroundResource(R.color.white);
		tvPlayG.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvPlayG);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvGolfC = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvGolfC.setText(getString(R.string.golf_c) + ": " + golfC);
		tvGolfC.setTextSize(18);
		tvGolfC.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvGolfC.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvGolfC.setBackgroundResource(R.color.white);
		tvGolfC.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvGolfC);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvFitEq = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvFitEq.setText(getString(R.string.fit_eq) + ": " + fitEq);
		tvFitEq.setTextSize(18);
		tvFitEq.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvFitEq.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvFitEq.setBackgroundResource(R.color.white);
		tvFitEq.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvFitEq);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvFeeCarPark = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvFeeCarPark.setText(getString(R.string.fac_par) + ": " + feeCarPark);
		tvFeeCarPark.setTextSize(18);
		tvFeeCarPark.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvFeeCarPark.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvFeeCarPark.setBackgroundResource(R.color.white);
		tvFeeCarPark.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvFeeCarPark);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));

		TextView tvDisabCarPark = new TextView(this);
		//Log.e("WEEK", chkWeek(cleanDay));
		tvDisabCarPark.setText(getString(R.string.fac_dis_par) + ": " + disabCarPark + " (" + getString(R.string.disab) + ")");
		tvDisabCarPark.setTextSize(18);
		tvDisabCarPark.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvDisabCarPark.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvDisabCarPark.setBackgroundResource(R.color.white);
		tvDisabCarPark.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvDisabCarPark);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
	}
	
	public void siteInfo(String siteEngName, String siteName, String districtName, String regionName, String location, String tel, String officeTel, 
			String pubBeach, String indoorF, String outdoorF, String sumSch, String winSch){
		generator.getAnalysis(siteEngName);
		
		llInfo = new LinearLayout(this);
		llInfo.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
		llInfo.setOrientation(1);
		
		llSiteInfo = new LinearLayout(this);
		llSiteInfo.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
		llSiteInfo.setOrientation(1);
		
		svInfo = new ScrollView(this);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT,1);
		svInfo.setLayoutParams(params);
		
		TextView tvSiteName = new TextView(this);
		tvSiteName.setText(siteName);
		tvSiteName.setTextSize(25);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvSiteName.setGravity(Gravity.CENTER);
		tvSiteName.setBackgroundResource(R.color.highLightBlue);
		tvSiteName.setTextColor(getResources().getColor(R.color.black));
		//llSiteInfo.addView(tvSiteName);

		TextView tvBInfo = new TextView(this);
		tvBInfo.setText(getString(R.string.bInfo));
		tvBInfo.setTextSize(22);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvBInfo.setGravity(Gravity.CENTER);
		tvBInfo.setBackgroundResource(R.color.titleBlue);
		tvBInfo.setTextColor(getResources().getColor(R.color.black));
		llSiteInfo.addView(tvBInfo);

		View line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvDistReg = new TextView(this);
		tvDistReg.setText(getString(R.string.district) + ": " + districtName + ", " + regionName);
		tvDistReg.setTextSize(18);
		tvDistReg.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvDistReg.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvDistReg.setBackgroundResource(R.color.white);
		tvDistReg.setTextColor(getResources().getColor(R.color.black));
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvDistReg);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvLocation = new TextView(this);
		
		String siteadd = getString(R.string.swim_address) + ": ";
		Spannable towedToAddress = new SpannableString(siteadd + location);
        towedToAddress.setSpan(new UnderlineSpan(), 4,towedToAddress.length(), 4);
        towedToAddress.setSpan(new ForegroundColorSpan(0xFF2894FF), 4, towedToAddress.length(), 4);
        
		tvLocation.setText(towedToAddress);
		//
		tvLocation.setTextSize(18);
		tvLocation.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvLocation.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		//tvLocation.setBackgroundResource(R.color.white);
		tvLocation.setOnClickListener(beachDetail.this);
		//Linkify.addLinks(tvLocation, Linkify.MAP_ADDRESSES);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvLocation);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvTel = new TextView(this);
		//tvTel.setText(getString(R.string) + ": " + tel);
		if(officeTel.equals("null"))
			tvTel.setText(getString(R.string.tel) + ": " + tel);
		else
			tvTel.setText(getString(R.string.tel) + ": " + tel + " / " + officeTel);
			
		tvTel.setTextSize(18);
		tvTel.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvTel.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvTel.setBackgroundResource(R.color.white);
		tvTel.setTextColor(getResources().getColor(R.color.black));
		Linkify.addLinks(tvTel, Linkify.PHONE_NUMBERS);
		tvTel.setLinkTextColor(Color.parseColor("#2894FF"));
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvTel);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvPub = new TextView(this);

		if(pubBeach.equals("1"))
			tvPub.setText(getString(R.string.pub_beach) + ": " + getString(R.string.pub_yes));
		else
			tvPub.setText(getString(R.string.pub_beach) + ": " + getString(R.string.pub_no));
		
		tvPub.setTextSize(18);
		tvPub.setPadding(20, 0, 20, 0);
		//tvDistrict.setWidth(1);
		tvPub.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//tvDistrict.setGravity(Gravity.CENTER);
		tvPub.setBackgroundResource(R.color.white);
		tvPub.setTextColor(getResources().getColor(R.color.black));
		//Linkify.addLinks(tvTel, Linkify.PHONE_NUMBERS);
		//rlGc.addView(tvDistrict, layoutParams);
		llSiteInfo.addView(tvPub);

		line = new View(context);
		line.setBackgroundColor(getResources().getColor(R.color.lineBlue));
		llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		
		TextView tvSumSchInfo = new TextView(this);
		tvSumSchInfo.setText(getString(R.string.sum_sch));
		tvSumSchInfo.setTextSize(22);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvSumSchInfo.setGravity(Gravity.CENTER);		
		
		if(sumSch.equals("null")){
			tvSumSchInfo.setBackgroundResource(R.color.gray);
			tvSumSchInfo.setTextColor(getResources().getColor(R.color.white));
			llSiteInfo.addView(tvSumSchInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		}
		else{
			tvSumSchInfo.setBackgroundResource(R.color.titleBlue);
			tvSumSchInfo.setTextColor(getResources().getColor(R.color.black));
			llSiteInfo.addView(tvSumSchInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
			sumInfo(chkSumSch(sumSch));
		}
		
		TextView tvWinSchInfo = new TextView(this);
		tvWinSchInfo.setText(getString(R.string.win_sch));
		tvWinSchInfo.setTextSize(22);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvWinSchInfo.setGravity(Gravity.CENTER);		
		
		if(winSch.equals("null")){
			tvWinSchInfo.setBackgroundResource(R.color.gray);
			tvWinSchInfo.setTextColor(getResources().getColor(R.color.white));
			llSiteInfo.addView(tvWinSchInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		}
		else{
			tvWinSchInfo.setBackgroundResource(R.color.titleBlue);
			tvWinSchInfo.setTextColor(getResources().getColor(R.color.black));
			llSiteInfo.addView(tvWinSchInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
			winInfo(chkWinSch(winSch));
		}
		
		TextView tvOutInfo = new TextView(this);
		tvOutInfo.setText(getString(R.string.outdoor));
		tvOutInfo.setTextSize(22);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvOutInfo.setGravity(Gravity.CENTER);		
		
		if(outdoorF.equals("null")){
			tvOutInfo.setBackgroundResource(R.color.gray);
			tvOutInfo.setTextColor(getResources().getColor(R.color.white));
			llSiteInfo.addView(tvOutInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		}
		else{
			tvOutInfo.setBackgroundResource(R.color.titleBlue);
			tvOutInfo.setTextColor(getResources().getColor(R.color.black));
			llSiteInfo.addView(tvOutInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
			chkOutdoor(outdoorF);
		}
		
		TextView tvInInfo = new TextView(this);
		tvInInfo.setText(getString(R.string.indoor));
		tvInInfo.setTextSize(22);
		//textGcName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
		//layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvInInfo.setGravity(Gravity.CENTER);		
		
		if(indoorF.equals("null")){
			tvInInfo.setBackgroundResource(R.color.gray);
			tvInInfo.setTextColor(getResources().getColor(R.color.white));
			//llSiteInfo.addView(tvInInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			//llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
		}
		else{
			tvInInfo.setBackgroundResource(R.color.titleBlue);
			tvInInfo.setTextColor(getResources().getColor(R.color.black));
			llSiteInfo.addView(tvInInfo);
			line = new View(context);
			line.setBackgroundColor(getResources().getColor(R.color.lineTitleBlue));
			llSiteInfo.addView(line, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5));
			indoorFInfo(chkIndoorF(indoorF));
		}
		if(generator.isOnline()){
			ll = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.details_swim, null);
			ImageView ivSwim = (ImageView) ll.findViewById(R.id.ivSwim);
			mImageUrl = new String []{
					getString(R.string.beach_image_url) +"S-%20"+ siteEngName.replaceAll(" ", "%20")+".jpg",
					getString(R.string.beach_image_url) +"S-%20" + siteEngName.replaceAll(" ", "%20")+".JPG",
					getString(R.string.beach_image_url) + "SK-%20" + siteEngName.replaceAll(" ", "%20") + ".JPG",
					getString(R.string.beach_image_url) + "TM-" + siteEngName.replaceAll(" ", "%20") + ".JPG"    	       
	        };
			new DownloadImageTask(ivSwim).execute(mImageUrl[mCount]);
		}
		svInfo.addView(llSiteInfo);
		llInfo.addView(tvSiteName);
		llInfo.addView(svInfo);
		setContentView(llInfo);
	}
	
	private boolean showImage(String urldisplay, ImageView ivSwim, LinearLayout ll){

        Bitmap mIcon11 = null;
        try {
	        InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
            in.close();
            ivSwim.setImageBitmap(mIcon11);
			llSiteInfo.addView(ll);
			return true;
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            return false;
        }
	}
	
	class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	      ImageView bmImage;

	      public DownloadImageTask(ImageView bmImage) {
	          this.bmImage = bmImage;
	      }

	      protected Bitmap doInBackground(String... urls) {
	          String urldisplay = urls[0];
	          Bitmap mIcon11 = null;
	          try {
	            InputStream in = new java.net.URL(urldisplay).openStream();
	            mIcon11 = BitmapFactory.decodeStream(in);
	          } catch (Exception e) {
	              Log.e("Error", e.getMessage());
	          }
	          return mIcon11;
	      }

	      protected void onPostExecute(Bitmap result) {
	          if(result != null){
		          bmImage.setImageBitmap(result);
		          llSiteInfo.addView(ll);
	          }else{
	        	  if(mImageUrl.length > mCount+1){
	        		  ImageView ivSwim = (ImageView) ll.findViewById(R.id.ivSwim);
	        		  mCount++;
	        		  new DownloadImageTask(ivSwim).execute(mImageUrl[mCount]);
	        	  }
	          }
	      }
	    }

	@Override
	public void onClick(View v) {
		/*Intent intent = new Intent(beachDetail.this, beachDetail.class);
    	intent.putExtra("siteid", siteid[position]);
		startActivity(intent);*/
		//Log.e("LL", latitude+","+longitude);
		Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + latitude + "," + longitude + "?q=<" + latitude + ">,<" + longitude + ">&z=18"));
	    startActivity(searchAddress);
		
	}
	
}