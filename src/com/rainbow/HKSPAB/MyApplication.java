package com.rainbow.HKSPAB;

import android.app.Application;
import android.content.Context;

public class MyApplication extends Application
{
    public void onCreate(){
        super.onCreate();
        MainActivity.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MainActivity.context;
    }
    
    public static Context getSearchSwimContext()
    {
    	return SearchSwim.context;
    }
}
