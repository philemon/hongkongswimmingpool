package com.rainbow.HKSPAB;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.GAServiceManager;
import com.google.analytics.tracking.android.Tracker;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class FuncGenerator
{
	 private double samplerate;  // determine how much information to be send
	 private int dispatchtime; // determine how long send the information to Google
     private int mDbCount = 0;
	 
    public FuncGenerator() 
    {
    	initDataValue();
    }
    
    private void initDataValue()
    {
		samplerate = 100;
		dispatchtime = 3;
    }
    
    public void callGovHKNonFunc() 
	{
    	new Thread(new Runnable()
    	{
			public void run() 
			{
				StringBuilder builder = new StringBuilder();
				HttpGet myget = new HttpGet(MyApplication.getAppContext().getString(R.string.govhknotiUrl));
				HttpParams httpParameters = new BasicHttpParams();
				try 
				{
				    int timeoutConnection = 800;
				    HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
				    // Set the default socket timeout (SO_TIMEOUT) 
				    // in milliseconds which is the timeout for waiting for data.
				    int timeoutSocket = 1000;
				    HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
				    DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
					HttpResponse response = httpClient.execute(myget);
					BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
					for (String s = reader.readLine(); s != null; s = reader.readLine()) 
					{
						builder.append(s);
					}
					//Linkify.addLinks(s, Linkify.ALL);
					Log.d("builder", builder.toString().substring(26));
					List<GovHKInfo> hkinfo = new ArrayList<GovHKInfo>();
			        JSONArray jArray = new JSONArray(builder.toString().substring(26));
					for (int i = 0; i < jArray.length(); i++) 
					{
			        	JSONObject jsondata = jArray.getJSONObject(i);
			        	if(jsondata.getString("source_id").substring(0, 3).equals("LCS") || jsondata.getString("source_id").substring(0, 3).equals("HKO"))
			        	{
			        		hkinfo.add(new GovHKInfo(
			        	 		Double.valueOf(jsondata.getString("id")),
			        	 		jsondata.getString("subject"),
			        	 		Double.valueOf(jsondata.getString("schedule_ts")),
			        	 		Double.valueOf(jsondata.getString("exp_ts")),
			        	 		jsondata.getString("source_id")
			        	 		));
			        	} 
			        }
			        for (int i = 0; i < hkinfo.size(); i++) 
			        {
			        	 Log.d("govhknoti", "subject:" + hkinfo.get(i).getSubject() + 
			        		" schedule_ts:" + convertMillsecToDate(hkinfo.get(i).getScheduleTs()) + " " + convertMillsecToDate(hkinfo.get(i).getExpTs()) + 
			        		" source_id:" + hkinfo.get(i).getSourceID());
					}
				} 
				catch (Exception e) 
				{
					Log.e("url response", e.getMessage());
					e.printStackTrace();
				}
				finally{
					Intent intent = new Intent();
		            intent.setAction(MainActivity.FINISH_GOV_NON);
		            MyApplication.getAppContext().sendBroadcast(intent);
				}
			}
    	}).start();	
	}
    
	public boolean isOnline()
	{ 
	    ConnectivityManager cm = (ConnectivityManager) MyApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE); 
	    NetworkInfo netInfo = cm.getActiveNetworkInfo(); 
	    if (netInfo != null && netInfo.isConnectedOrConnecting()) return true; 
	    else return false; 
	}

	public void createSPABTable() 
	{
		if(!SQLiteDao.checkTableExists("bh_facilities_indoor"))
		{
			SQLiteDao.Create.createBeachIndoorFacTable();
			getJSONData("bh_facilities_indoor");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("bh_facilities_outdoor"))
		{
			SQLiteDao.Create.createBHOutdoorFacTable();
			getJSONData("bh_facilities_outdoor");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("bh_lifeguard_service_time_summer"))
		{
			SQLiteDao.Create.createBHLGServSMTimeTable();
			getJSONData("bh_lifeguard_service_time_summer");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("bh_lifeguard_service_time_winter"))
		{
			SQLiteDao.Create.createBHLGServWTTimeTable();
			getJSONData("bh_lifeguard_service_time_winter");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("bh_other_info"))
		{
			SQLiteDao.Create.createBHOtherInfoTable();
			getJSONData("bh_other_info");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("hk_district"))
		{
			SQLiteDao.Create.createHKDistrictTable();
			getJSONData("hk_district");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("hk_region"))
		{
			SQLiteDao.Create.createHKRegionTable();
			getJSONData("hk_region");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("site"))
		{
			SQLiteDao.Create.createSiteTable();
			getJSONData("site");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("site_location"))
		{
			SQLiteDao.Create.createSiteLocationTable();
			getJSONData("site_location");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("site_photos"))
		{
			SQLiteDao.Create.createSitePhotoTable();
			getJSONData("site_photos");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("sp_facilities_indoor"))
		{
			SQLiteDao.Create.createSPIndoorFacTable();
			getJSONData("sp_facilities_indoor");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("sp_facilities_other"))
		{
			SQLiteDao.Create.createSPOtherFacTable();
			getJSONData("sp_facilities_other");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("sp_facilities_outdoor"))
		{
			SQLiteDao.Create.createSPOutdoorFacTable();
			getJSONData("sp_facilities_outdoor");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("sp_open_schedule_summer"))
		{
			SQLiteDao.Create.createSPSMrOpenScheduleTable();
			getJSONData("sp_open_schedule_summer");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("sp_open_schedule_winter"))
		{
			SQLiteDao.Create.createSPWTOpenScheduleTalbe();
			getJSONData("sp_open_schedule_winter");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("sp_other_info"))
		{
			SQLiteDao.Create.createSPOtherInfoTable();
			getJSONData("sp_other_info");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("update_info"))
		{
			SQLiteDao.Create.createUpdateInfoTable();
			getJSONData("update_info");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("week"))
		{
			SQLiteDao.Create.createWeekTable();
			getJSONData("week");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("info_comment"))
		{
			SQLiteDao.Create.createInfoNomment();
			getJSONData("info_comment");
		}else{
			mDbCount++;
		}
		if(!SQLiteDao.checkTableExists("info_relationship"))
		{
			SQLiteDao.Create.createInfoRelationship();
			getJSONData("info_relationship");
		}else{
			mDbCount++;
		}
		if(mDbCount == 20){
			checkUpdataTable();
		}
	}
	
	public void checkUpdataTable() 
	{
    	new Thread(new Runnable()
    	{
			public void run() 
			{
				boolean isupdata = false;
				HttpClient client = new DefaultHttpClient();
				StringBuilder builder = new StringBuilder();
				HttpGet myget = new HttpGet(MyApplication.getAppContext().getString(R.string.server_url)+"JSONphp/select.php?table_name=update_info");
				Log.d("HttpGet", MyApplication.getAppContext().getString(R.string.server_url)+"JSONphp/select.php?table_name=update_info");
				try 
				{
					HttpResponse response = client.execute(myget);
					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					for (String s = reader.readLine(); s != null; s = reader.readLine()) 
					{
						builder.append(s);
					}
			         JSONArray jarray = new JSONArray(builder.toString());
			         for (int i = 0; i < jarray.length(); i++) 
			         {
			        	 JSONObject jsondata = jarray.getJSONObject(i);
			        	 Log.d("last_update", jsondata.getInt("last_update")+" "+SQLiteDao.Search.searchSPABUpdateTime(jsondata.getString("table_name")));
			        	 if(jsondata.getInt("last_update")!= SQLiteDao.Search.searchSPABUpdateTime(jsondata.getString("table_name")) &&
			        			 SQLiteDao.Search.searchSPABUpdateTime(jsondata.getString("table_name")) != -1)
			        	 {
			        		 	SQLiteDao.Drop.dropSPABTable(jsondata);
			        		    Log.d("update table", jsondata.getString("table_name"));
			        		    SQLiteDao.Update.updateSPABTable(jsondata);
			        		    isupdata = true;
			        	 }
			        	 if(isupdata)
			        	 {
			        		 createSPABTable();
			        	 }
			        	 /*table_name.add(json_data.getString("table_name"));
			        	 comment.add(json_data.getString("comment"));
			        	 last_upata.add(json_data.getString("last_upata"));	   */     	 
			         }
				} 
				catch (Exception e) 
				{
					Log.e("update url response", e.getMessage());
					e.printStackTrace();
				}
				finally{
					Intent intent = new Intent();
		            intent.setAction(MainActivity.FINISH_CHECK_UPDATE);
		            MyApplication.getAppContext().sendBroadcast(intent);	
				}
			}
    	}).start();
		
	}

	public void getAnalysis(String activityName) 
	{
	      EasyTracker.getInstance().setContext(MyApplication.getAppContext()); // initial
	      Tracker tracker = EasyTracker.getTracker();// use for track event, exception..etc
	      tracker.setSampleRate(samplerate);
	      GAServiceManager.getInstance().setDispatchPeriod(dispatchtime);
	      tracker.trackEvent("UI_ACTION", "buttonClick", "labelCreatedByKen", 0l);
	      tracker.trackView(activityName);
	      tracker.trackException("ExceptionCreatedByRainbow", false);
	}
	
	private void getJSONData(final String dbname) 
	{
		 new Thread(new Runnable() 
		 {
			public void run() 
			{
				HttpClient client = new DefaultHttpClient();
				StringBuilder builder = new StringBuilder();
				
				HttpGet myget = new HttpGet(MyApplication.getAppContext().getString(R.string.server_url)+"JSONphp/select.php?table_name="+dbname);
				try 
				{
					HttpResponse response = client.execute(myget);
					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					for (String s = reader.readLine(); s != null; s = reader.readLine()) 
					{
						builder.append(s);
					}
			         JSONArray jArray = new JSONArray(builder.toString().replaceAll("'", "''"));
			        
			         for (int i = 0; i < jArray.length(); i++) 
			         {
			        	 JSONObject jsondata = jArray.getJSONObject(i);
			        	 insertTable(dbname,jsondata);
			         }
				} 
				catch (Exception e) 
				{
					Log.e("url response", "false");
					e.printStackTrace();
				}
				finally{
					mDbCount++;
					if(mDbCount == 20){
						checkUpdataTable();
					}
				}
			}

			private void insertTable(String dbname, JSONObject jsondata) 
			{
				if(dbname.equals("bh_facilities_indoor")){
					try 
					{
						SQLiteDao.Insert.insertBeachIndoorFacTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				if(dbname.equals("bh_facilities_outdoor"))
				{
					try 
					{
						SQLiteDao.Insert.insertBHOutdoorFacTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("bh_lifeguard_service_time_summer"))
				{
					try 
					{
						SQLiteDao.Insert.insertBHLGServSMTimeTable(jsondata);					
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					} 				
				}
				else if(dbname.equals("bh_lifeguard_service_time_winter"))
				{
					try 
					{
						SQLiteDao.Insert.insertBHLGServWTTimeTable(jsondata);	
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("bh_other_info"))
				{
					try 
					{
						SQLiteDao.Insert.insertBHOtherInfoTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}			
				}
				else if(dbname.equals("hk_district"))
				{
					try 
					{
						SQLiteDao.Insert.insertHKDistrictTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("hk_region"))
				{
					try 
					{
						SQLiteDao.Insert.insertHKRegionTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("site"))
				{
					try 
					{
						SQLiteDao.Insert.insertSiteTable(jsondata);
					} 
					catch (JSONException e)
					 {
						e.printStackTrace();
					}
						
				}
				else if(dbname.equals("site_location"))
				{
					try 
					{
						SQLiteDao.Insert.insertSiteLocationTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("site_photos"))
				{
					try 
					{
						SQLiteDao.Insert.insertSitePhotoTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("sp_facilities_indoor"))
				{
					try 
					{
						SQLiteDao.Insert.insertSPIndoorFacTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("sp_facilities_other"))
				{
					try 
					{
						SQLiteDao.Insert.insertSPOtherFacTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("sp_facilities_outdoor"))
				{
					try 
					{
						SQLiteDao.Insert.insertSPOutdoorFacTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("sp_open_schedule_summer"))
				{
					try 
					{
						SQLiteDao.Insert.insertSPSMrOpenScheduleTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("sp_open_schedule_winter"))
				{
					try 
					{
						SQLiteDao.Insert.insertSPWTOpenScheduleTalbe(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("sp_other_info"))
				{
					try 
					{
						SQLiteDao.Insert.insertSPOtherInfoTable(jsondata);
					}
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("week"))
				{
					try 
					{
						SQLiteDao.Insert.insertWeekTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("update_info"))
				{
					try 
					{
						SQLiteDao.Insert.insertUpdateInfoTable(jsondata);
					} 
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("info_comment"))
				{
					try
					{
						SQLiteDao.Insert.insertInfoComment(jsondata);
					}
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
				else if(dbname.equals("info_relationship"))
				{
					try
					{
						SQLiteDao.Insert.insertInfoRelationship(jsondata);
					}
					catch (JSONException e) 
					{
						e.printStackTrace();
					}
				}
			}	
		}).start();	
	}
	
		
    @SuppressLint("SimpleDateFormat")
	private String convertMillsecToDate(Double double1)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1970, 0, 1, 0, 0, 0);
        calendar.add(Calendar.SECOND, (int) (double1/1000));
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss MM/dd/yyyy EEEE");
    	return dateFormat.format(calendar.getTime());
    }
	
    public void createDatabaseFolder(){
    	File dir = new File(MyApplication.getAppContext().getString(R.string.database_path));
    	if(!dir.exists() || !dir.isDirectory()) {
    		dir.mkdir();
    	}
    }
    
    public void setAdmob(AdView adView, LinearLayout layout, Context context){
		adView = new AdView(context);
		adView.setAdUnitId("ca-app-pub-4668589427063896/2127835569");
		adView.setAdSize(AdSize.SMART_BANNER);
		layout.addView(adView);
		adView.loadAd(new AdRequest.Builder().build());
    }
    
}