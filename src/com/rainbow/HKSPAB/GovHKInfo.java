package com.rainbow.HKSPAB;

public class GovHKInfo
{
	private Double id;
	private String subject;
	private Double schedulets;
	private Double expts;
	private String sourceid;
	
	public GovHKInfo(Double id, String subject, Double schedulets, Double expts, String sourceid)
	{
		this.id = id;
		this.subject = subject;
		this.schedulets = schedulets;
		this.expts = expts;
		this.sourceid = sourceid;
	}
	
	public String getSubject() { return subject; }
	public Double getID() { return id; }
	public Double getScheduleTs() { return schedulets; }
	public Double getExpTs() { return expts; }	 
	public String getSourceID() { return sourceid; }   
}