package com.rainbow.HKSPAB;

import com.crashlytics.android.Crashlytics;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import com.google.android.gms.ads.AdView;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("SdCardPath")

public class MainActivity extends Activity implements OnClickListener 
{
	 public static final String FINISH_CHECK_UPDATE = "FINISH_CHECK_UPDATE";
	 public static final String FINISH_GOV_NON = "FINISH_GOV_NON";
	 
	 private static final String TAG = MainActivity.class.getName();
	 private FuncGenerator generator;
	 
	 private final int CONTACT_US = 0x111;
	 private final int LANGUAGE_EN = 0x112;
	 private final int LANGUAGE_ZH = 0x113;
	 private final int SWIM_NOTICE = 0x114;
	 
	 private ProgressDialog mProgressDialog;
	 private ImageView ivBeach,ivSwimming;
	 private TextView  tvBeach, tvSwimminng;
	 
	 public static Context context;
	 private SharedPreferences splanguage;
	 private boolean mIsCheckUpdateFinish = false;
	 private boolean mIsCheckGovNonFinish = false;
	 private String mLang = "en";
	 private AdView adView;
	 
	 private Timer timer = new Timer(true);
	
	protected void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
		Crashlytics.start(this);
        mProgressDialog = ProgressDialog.show(this, "", getString(R.string.Progress));  
        mProgressDialog.setCancelable(true);
        timer.schedule(new timerTask(), 5000, 1000);
        mProgressDialog.setOnCancelListener(new OnCancelListener(){
           @Override
           public void onCancel(DialogInterface dialog){
        	   dialog.dismiss();
        }});
        initConfigLocale();
		if (!splanguage.getBoolean("AGREE", false)){
			ShowAlertDialogAndButton();
		}
        initDataValue();
        initEventHandler();
        initFunction();	
        LinearLayout layout = (LinearLayout) findViewById(R.id.mainLayout);
        generator.setAdmob(adView, layout, context);
	}
	
	  public class timerTask extends TimerTask
	  {
	    public void run()
	    {
	    	if(mProgressDialog != null && mProgressDialog.isShowing()){
	    		mProgressDialog.dismiss();
	    	}
	    }
	  };
	
	protected void onDestroy(){
		unregisterReceiver(mBroadcast);
		ivBeach.setOnClickListener(null);
		ivSwimming.setOnClickListener(null);
		tvBeach.setOnClickListener(null);
		tvSwimminng.setOnClickListener(null);
		super.onDestroy();
	}
	
	@Override
	public void onPause() {
		if (adView != null)
			adView.pause();
		super.onPause();
	}
	@Override
	public void onResume() {
		super.onResume();
		if (adView != null)
			adView.resume();
	}

	private void ShowAlertDialogAndButton() {
		AlertDialog.Builder MyAlertDialog = new AlertDialog.Builder(this);
		MyAlertDialog.setTitle(getString(R.string.disclaimer));
		MyAlertDialog.setMessage(getString(R.string.disclaimer_msg));
		MyAlertDialog.setCancelable(false);
		// �إ߫��U���s
		DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (which == -1) {
					finish();
					android.os.Process.killProcess(android.os.Process.myPid());
				} else {
					Editor editor = splanguage.edit();
					editor.putBoolean("AGREE", true);
					editor.commit();
				}
			}
		};
		MyAlertDialog.setPositiveButton(getString(R.string.exit), OkClick);
		MyAlertDialog.setNeutralButton(getString(R.string.agree), OkClick);
		MyAlertDialog.show();

	}
	private void initDataValue()
	{
		setContentView(R.layout.activity_main);
		
		ivBeach = (ImageView) findViewById(R.id.ivBeach);
		ivSwimming = (ImageView) findViewById(R.id.ivSwimming);
		tvBeach = (TextView) findViewById(R.id.tvBeach);
		tvSwimminng = (TextView) findViewById(R.id.tvSwimming);		
	}
	
	private void initEventHandler()
	{
		// Change Scene action when user click beach or swim icon or text
		ivBeach.setOnClickListener(this);
		ivSwimming.setOnClickListener(this);
		tvBeach.setOnClickListener(this);
		tvSwimminng.setOnClickListener(this);
	}
	
	private void initFunction()
	{
		IntentFilter intentFilter = new IntentFilter(FINISH_CHECK_UPDATE);
		intentFilter.addAction(FINISH_GOV_NON);
		this.registerReceiver(mBroadcast, intentFilter);
		generator.getAnalysis("MainActivity");
		generator.createDatabaseFolder();
		callAssetDB(); //Sqlite database from asset folder Android
		if(generator.isOnline())
		{
			generator.createSPABTable();
			generator.callGovHKNonFunc();
		}else{
			mProgressDialog.dismiss();
		}
	}
		
	private BroadcastReceiver mBroadcast = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (context == null || intent == null) {
				return;
			}
			if(intent.getAction().equals(FINISH_GOV_NON)){
				mIsCheckGovNonFinish = true;
				Log.d(TAG,"mIsCheckGovNonFinish = true");
			}else if(intent.getAction().equals(FINISH_CHECK_UPDATE)){
				 mIsCheckUpdateFinish = true;
				 Log.d(TAG ,"mIsCheckUpdateFinish = true");
			}
			if(mProgressDialog != null && mIsCheckGovNonFinish && mIsCheckUpdateFinish){
				mProgressDialog.dismiss();
			}
		}
	};
	
	public void callAssetDB() 
	{
	    // CHECK IS EXISTS OR NOT
		File f = new File(
				getString(R.string.database_path)+getString(R.string.database_name));
		Log.d(TAG, "f: " + f.exists() + "");
		if (!f.exists()) {
			try
			{
			    // COPY IF NOT EXISTS
			    AssetManager am = getApplicationContext().getAssets();
			    OutputStream os = new FileOutputStream(getString(R.string.database_path)+getString(R.string.database_name));
			    byte[] b = new byte[100];
			    int r;
			    InputStream is = am.open(getString(R.string.database_name));
			    while ((r = is.read(b)) != -1) 
			    {
			    	os.write(b, 0, r);
		   		}
		   		is.close();
		   		os.close();
			}
			catch(Exception e1)
			{
				Log.e(TAG, e1.getMessage());
			}	
		}
	}
	
	private void initConfigLocale()
	{
		context = MainActivity.this;
		generator = new FuncGenerator();  
		splanguage = context.getSharedPreferences("LANGUAGE", MODE_PRIVATE);
		Configuration config = new Configuration(getResources().getConfiguration());
        if(splanguage.getInt("LANGUAGE_ID", -1)==1) {
        	config.locale = Locale.ENGLISH;
        }
        else if(splanguage.getInt("LANGUAGE_ID", -1)==2){
        	config.locale = Locale.CHINESE;
        	mLang = "zh";
        }else{
        	if(Locale.getDefault().getLanguage().equals("zh")){
        		config.locale = Locale.CHINESE;
        		mLang = "zh"; 
        	}
        }
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
	}	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		menu.add(0,SWIM_NOTICE,0,getString(R.string.swim_notice));
		menu.add(0, CONTACT_US, 0, getString(R.string.contact_us)).setIcon(R.drawable.ic_launcher);
		SubMenu languagemenu = menu.addSubMenu(getString(R.string.language)).setIcon(R.drawable.ic_launcher);
		languagemenu.add(0,LANGUAGE_EN,0,getString(R.string.english));
		languagemenu.add(0,LANGUAGE_ZH,0,getString(R.string.chinese));
		//getMenuInflater().inflate("ContactUs"); 
		return true;
	}
	
	@SuppressLint("CommitPrefEdits")
	@Override
	public boolean onOptionsItemSelected(MenuItem mi)
	{
        Editor editor = splanguage.edit();
        Intent intent;
		switch (mi.getItemId())
		{
			case CONTACT_US:
				Intent openEmailIntent = new Intent(
						android.content.Intent.ACTION_SEND);
				openEmailIntent.setType("plain/text");
				openEmailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
						new String[] { "rainbowdeveloperltd@gmail.com" });
				openEmailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
						"Report Swimming Going problem");
				this.startActivity(Intent.createChooser(openEmailIntent,
						"Send Email"));
				break;
			case LANGUAGE_EN:
		        editor.putInt("LANGUAGE_ID", 1);
		        editor.commit();
		        intent = new Intent(this,MainActivity.class);
		        startActivity(intent);
		        this.finish();
				break;
			case LANGUAGE_ZH:
		        editor.putInt("LANGUAGE_ID", 2);
		        editor.commit();
		        intent = new Intent(this,MainActivity.class);
		        startActivity(intent);
		        this.finish();
			    break;
			case SWIM_NOTICE:
				if(!generator.isOnline()){
					Toast.makeText(this, R.string.no_network, Toast.LENGTH_SHORT).show();
					break;
				}
				CharSequence colors[] = new CharSequence[] 
						{getString(R.string.hki_kln), getString(R.string.nt)};

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(R.string.swim_notice);
				builder.setItems(colors, new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int which) {
				    	Intent myIntent = new Intent(MainActivity.this, SwimNotice.class);
				        if(which == 0){
				        	myIntent.putExtra("url", "Hong_Kong Island_"+mLang+".html"); 
				        }else{
				        	myIntent.putExtra("url", "New Territories_"+mLang+".html"); 
				        }
				    	startActivity(myIntent);
				    }
				});
				builder.show();
				break;
		}
		return true;
	}
	
	//Handle all on click control
	@Override
	public void onClick(View v) 
	{
		Intent intent = new Intent();
		if(v == ivBeach || v == tvBeach) intent.setClass(this, SearchBeach.class);
		else if(v == ivSwimming || v == tvSwimminng) intent.setClass(this, SearchSwim.class);
		startActivity(intent);
	}
}
